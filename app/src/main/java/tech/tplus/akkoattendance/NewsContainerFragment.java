package tech.tplus.akkoattendance;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class NewsContainerFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        assert getFragmentManager() != null;
        getFragmentManager().beginTransaction()
                .replace(R.id.news_container, new NewsFragment())
                .commit();
        return inflater.inflate(R.layout.fragment_news_container, container, false);
    }
}
