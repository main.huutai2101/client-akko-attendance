package tech.tplus.akkoattendance;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class HomeContainerFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        assert getFragmentManager() != null;
        getFragmentManager().beginTransaction()
                .replace(R.id.home_container, new HomeFragment())
                .commit();
        return inflater.inflate(R.layout.fragment_home_container, container, false);
    }
}
