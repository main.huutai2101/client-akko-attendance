package tech.tplus.akkoattendance;

import android.os.Bundle;
import android.widget.EditText;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import tech.tplus.akkoattendance.entity.Clazz;


public class ClassDetailsFragment extends Fragment {

    private final Clazz clazz;

    public ClassDetailsFragment(Clazz clazz) {
        this.clazz = clazz;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_class_details, container, false);

        EditText edCode = view.findViewById(R.id.edCode);
        EditText edDescription = view.findViewById(R.id.edDescription);

        edCode.setText(clazz.getCode());
        edDescription.setText(clazz.getDescription());

        return view;
    }
}
