package tech.tplus.akkoattendance;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.ResponseBody;
import org.modelmapper.ModelMapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tech.tplus.akkoattendance.adapter.MenuAdapter;
import tech.tplus.akkoattendance.adapter.StoryAdapter;
import tech.tplus.akkoattendance.controller.*;
import tech.tplus.akkoattendance.dto.StudentDto;
import tech.tplus.akkoattendance.dto.SubjectSemesterDto;
import tech.tplus.akkoattendance.dto.SubjectSessionDto;
import tech.tplus.akkoattendance.dto.SubjectSlotDto;
import tech.tplus.akkoattendance.entity.*;
import tech.tplus.akkoattendance.helper.API;
import tech.tplus.akkoattendance.helper.App;
import tech.tplus.akkoattendance.other.PopupStatus;
import tech.tplus.akkoattendance.other.PopupType;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class HomeFragment extends Fragment {
    private static final String TAG = HomeFragment.class.getSimpleName();

    View view;
    TextView welcomeCode, welcomeStatus, txtSlotTitle;
    ListView lvMenu;
    RecyclerView lvStory;

    private String teacherCode;
    private boolean isStudentDownloaded = false;
    private HomeFragListener listener;



    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof HomeFragment.HomeFragListener) {
            listener = (HomeFragment.HomeFragListener) context;
        } else {
            Log.d(TAG, "Cannot attach data to HomeActivity");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        listener.statusDownload(0,1);
        listener.subjectDownload(0,1);
        listener.classDownload(0,1);
        listener.majorDownload(0,1);
        listener.roomDownload(0,1);
        listener.slotDownload(0,1);
        listener.sessionDownload(0,1);
        listener.subjectSemesterDownload(0,1);
        listener.studentDownload(0,1);
        if (view == null) view = inflater.inflate(R.layout.fragment_home, container, false);
        if (App.isNetworkConnected(Objects.requireNonNull(getActivity()))) doSynchronized();
        else {
            listener.statusDownload(1,1);
            listener.subjectDownload(1,1);
            listener.classDownload(1,1);
            listener.majorDownload(1,1);
            listener.roomDownload(1,1);
            listener.slotDownload(1,1);
            listener.sessionDownload(1,1);
            listener.subjectSemesterDownload(1,1);
            listener.studentDownload(1,1);
        }
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        onFind();
        onInit();
    }



    /**
     * Create view when open the application
     */
    private void onInit() {
        //Get teacher code, and show welcome
        teacherCode = App.sqlite(Objects.requireNonNull(getActivity())).getMapEntityDao().load("teacher_code").getValue();
        String welcome = getResources().getString(R.string.hi) + ", " + teacherCode;
        welcomeCode.post(() -> welcomeCode.setText(welcome));

        //Set status (random for datasource)
        String[] status = getResources().getStringArray(R.array.status_array);
        welcomeStatus.post(() -> welcomeStatus.setText(status[new Random().nextInt(status.length)]));

        //Init view for menu
        MenuAdapter menuAdapter = new MenuAdapter(getContext());
        lvMenu.post(() -> {
            lvMenu.setAdapter(menuAdapter);
            App.setListViewHeightBasedOnChildren(lvMenu);
        });
        lvMenu.setOnItemClickListener((parent, view, position, id) -> {
            assert getFragmentManager() != null;
            switch (position) {
                case 0: //Timetable
                    getFragmentManager().beginTransaction().add(R.id.home_container, new TimetableFragment()).addToBackStack(TAG).commit();
                    break;
                case 1: //Attendance
                    getFragmentManager().beginTransaction().add(R.id.home_container, new AttendanceFragment()).addToBackStack(TAG).commit();
                    break;
                case 2: //Report
                    new Popup(Objects.requireNonNull(getContext()), getString(R.string.coming_soon), getString(R.string.coming_content), PopupStatus.WARNING, PopupType.OK).show();
                    break;
                case 3: //Class
                    getFragmentManager().beginTransaction().add(R.id.home_container, new ClassFragment()).addToBackStack(TAG).commit();
                    break;
                case 4: //Subject
                    getFragmentManager().beginTransaction().add(R.id.home_container, new SubjectFragment()).addToBackStack(TAG).commit();
                    break;
            }


        });

        //Init view for story
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);

        //Get 5 list data
        List<SubjectSemester> subjectSemesterList = App.sqlite(Objects.requireNonNull(getActivity())).getSubjectSemesterDao().loadAll();
        List<SubjectSlot> slotList = App.sqlite(getActivity()).getSubjectSlotDao().loadAll();
        List<Clazz> clazzList = App.sqlite(getActivity()).getClazzDao().loadAll();
        List<Subject> subjectList = App.sqlite(getActivity()).getSubjectDao().loadAll();
        List<Room> roomList = App.sqlite(getActivity()).getRoomDao().loadAll();

        List<SubjectSlot> slotToday = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        subjectSemesterList.forEach(subjectSemester -> {
            //Filter slot
            List<SubjectSlot> temp = slotList.stream().filter(subjectSlot ->
                    subjectSlot.getSubjectSemesterId().equals(subjectSemester.getId()) &&
                    subjectSlot.getDate().getDate() == calendar.getTime().getDate() &&
                    subjectSlot.getDate().getMonth() == calendar.getTime().getMonth() &&
                    subjectSlot.getDate().getYear() == calendar.getTime().getYear()).collect(Collectors.toList());
            slotToday.addAll(temp);
        });
        Log.d(TAG, "Today, teacher has " + slotToday.size() + " slot(s)");

        StoryAdapter storyAdapter = new StoryAdapter();
        if (slotToday.size() != 0 && subjectList.size() !=0) {
            //Show title
            txtSlotTitle.post(() -> txtSlotTitle.setVisibility(View.VISIBLE));

            //Set adapter for story
            storyAdapter = new StoryAdapter(slotToday, clazzList, subjectList, roomList);
            storyAdapter.setItemClickListener((slot, position) -> {
                assert getFragmentManager() != null;
                getFragmentManager().beginTransaction().add(R.id.home_container, new StudentFragment(slot)).addToBackStack(TAG).commit();
            });
        }
        StoryAdapter temp = storyAdapter;
        lvStory.post(() -> {
            lvStory.setAdapter(temp);
            lvStory.setLayoutManager(layoutManager);
        });
    }

    /**
     * Find view
     */
    private void onFind() {
        welcomeCode = view.findViewById(R.id.txtWelcomeCode);
        welcomeStatus = view.findViewById(R.id.txtWelcomeStatus);
        txtSlotTitle = view.findViewById(R.id.txt_slot_today);
        lvMenu = view.findViewById(R.id.lvMenu);
        lvStory = view.findViewById(R.id.lvStory);
    }

    public interface HomeFragListener {
        void homeFrag(boolean isOpenSlot);
        void statusDownload(int process, int size);
        void subjectDownload(int process, int size);
        void classDownload(int process, int size);
        void majorDownload(int process, int size);
        void roomDownload(int process, int size);
        void slotDownload(int process, int size);
        void sessionDownload(int process, int size);
        void subjectSemesterDownload(int process, int size);
        void studentDownload(int process, int size);
    }

    /**
     * doSynchronized with 3 AsyncTasks
     * 1. Get basic data: status, subjects, class, majors, rooms and all slots
     * 2. Get all subject semester and collect class list, after that download data of Student
     * 3. Waiting for subject semester done, download all information of student list
     */
    @SuppressLint("StaticFieldLeak")
    private void doSynchronized() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                //Get all Status from Sever
                API.build(getContext()).create(StatusController.class).getAll().enqueue(new Callback<List<tech.tplus.akkoattendance.entity.Status>>() {
                    @Override
                    public void onResponse(Call<List<tech.tplus.akkoattendance.entity.Status>> call, Response<List<tech.tplus.akkoattendance.entity.Status>> response) {
                        if (response.code() == 200) {
                            new AsyncTask<Void, Void, Void>() {
                                @Override
                                protected Void doInBackground(Void... voids) {
                                    assert response.body() != null;
                                    AtomicInteger i = new AtomicInteger(0);
                                    response.body().forEach(status -> {
                                        i.incrementAndGet();
                                        listener.statusDownload(i.get() ,response.body().size());
                                        App.sqlite(Objects.requireNonNull(getActivity())).insertOrReplace(status);
                                    });
                                    Log.d(TAG, tech.tplus.akkoattendance.entity.Status.class.getSimpleName() + " - Data saved!");
                                    return null;
                                }
                            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        } else {
                            listener.statusDownload(1,1);
                            Log.d(TAG, tech.tplus.akkoattendance.entity.Status.class.getSimpleName() + " - Response Code: " + response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<List<tech.tplus.akkoattendance.entity.Status>> call, Throwable t) {
                        Log.d(TAG, tech.tplus.akkoattendance.entity.Status.class.getSimpleName() + " - Cannot connect to the Server!");
                        listener.statusDownload(1,1);
                    }
                });

                //Get all Subject from Server
                API.build(getContext()).create(SubjectController.class).getAllByTeacherCode(teacherCode).enqueue(new Callback<List<Subject>>() {
                    @Override
                    public void onResponse(Call<List<Subject>> call, Response<List<Subject>> response) {
                        if (response.code() == 200) {
                            new AsyncTask<Void, Void, Void>() {
                                @Override
                                protected Void doInBackground(Void... voids) {
                                    assert response.body() != null;
                                    AtomicInteger i = new AtomicInteger(0);
                                    response.body().forEach(subject -> {
                                        i.incrementAndGet();
                                        listener.subjectDownload(i.get(), response.body().size());
                                        App.sqlite(Objects.requireNonNull(getActivity())).insertOrReplace(subject);
                                    });
                                    Log.d(TAG, Subject.class.getSimpleName() + " - Data saved!");
                                    return null;
                                }
                            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        } else {
                            listener.subjectDownload(1,1);
                            Log.d(TAG, Subject.class.getSimpleName() + " - Response Code: " + response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Subject>> call, Throwable t) {
                        listener.subjectDownload(1,1);
                        Log.d(TAG, Subject.class.getSimpleName() + " - Cannot connect to the Server!");
                    }
                });

                //Get all Class from Server
                API.build(getContext()).create(ClazzController.class).getAllByTeacherCode(teacherCode).enqueue(new Callback<List<Clazz>>() {
                    @Override
                    public void onResponse(Call<List<Clazz>> call, Response<List<Clazz>> response) {
                        if (response.code() == 200) {
                            new AsyncTask<Void, Void, Void>() {
                                @Override
                                protected Void doInBackground(Void... voids) {
                                    assert response.body() != null;
                                    AtomicInteger i = new AtomicInteger(0);
                                    response.body().forEach(clazz -> {
                                        i.incrementAndGet();
                                        listener.classDownload(i.get(), response.body().size());
                                        App.sqlite(Objects.requireNonNull(getActivity())).insertOrReplace(clazz);
                                    });
                                    Log.d(TAG, Clazz.class.getSimpleName() + " - Data saved!");
                                    return null;
                                }
                            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        } else {
                            listener.classDownload(1,1);
                            Log.d(TAG, Clazz.class.getSimpleName() + " - Response Code: " + response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Clazz>> call, Throwable t) {
                        Log.d(TAG, Clazz.class.getSimpleName() + " - Cannot connect to the Server!");
                        listener.classDownload(1,1);
                    }
                });

                //Get all Major from Server
                API.build(getContext()).create(MajorController.class).getAll().enqueue(new Callback<List<Major>>() {
                    @Override
                    public void onResponse(Call<List<Major>> call, Response<List<Major>> response) {
                        if (response.code() == 200) {
                            new AsyncTask<Void, Void, Void>() {
                                @Override
                                protected Void doInBackground(Void... voids) {
                                    assert response.body() != null;
                                    AtomicInteger i = new AtomicInteger(0);
                                    response.body().forEach(major -> {
                                        i.incrementAndGet();
                                        listener.majorDownload(i.get(),response.body().size());
                                        App.sqlite(Objects.requireNonNull(getActivity())).insertOrReplace(major);
                                    });
                                    Log.d(TAG, Major.class.getSimpleName() + " - Data saved!");
                                    return null;
                                }
                            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        } else {
                            listener.majorDownload(1,1);
                            Log.d(TAG, Major.class.getSimpleName() + " - Response Code: " + response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Major>> call, Throwable t) {
                        listener.majorDownload(1,1);
                        Log.d(TAG, Major.class.getSimpleName() + " - Cannot connect to the Server!");
                    }
                });

                //Get all Room from Server
                API.build(getContext()).create(RoomController.class).getAll().enqueue(new Callback<List<Room>>() {
                    @Override
                    public void onResponse(Call<List<Room>> call, Response<List<Room>> response) {
                        if (response.code() == 200) {
                            new AsyncTask<Void, Void, Void>() {
                                @Override
                                protected Void doInBackground(Void... voids) {
                                    assert response.body() != null;
                                    AtomicInteger i = new AtomicInteger(0);
                                    response.body().forEach(room -> {
                                        i.incrementAndGet();
                                        listener.roomDownload(i.get(), response.body().size());
                                        App.sqlite(Objects.requireNonNull(getActivity())).insertOrReplace(room);
                                    });
                                    Log.d(TAG, Room.class.getSimpleName() + " - Data saved!");
                                    return null;
                                }
                            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        } else {
                            listener.roomDownload(1,1);
                            Log.d(TAG, Room.class.getSimpleName() + " - Response Code: " + response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Room>> call, Throwable t) {
                        listener.roomDownload(1,1);
                        Log.d(TAG, Room.class.getSimpleName() + " - Cannot connect to the Server!");
                    }
                });

                //Get all session
                API.build(getContext()).create(SessionController.class).getAllSessionByTeacherCode(teacherCode).enqueue(new Callback<List<SubjectSession>>() {
                    @Override
                    public void onResponse(Call<List<SubjectSession>> call, Response<List<SubjectSession>> response) {
                        if (response.code() == 200) {
                            new AsyncTask<Void, Void, Void>() {
                                @Override
                                protected Void doInBackground(Void... voids) {
                                    assert response.body() != null;
                                    AtomicInteger i = new AtomicInteger(0);
                                    response.body().forEach(session -> {
                                        i.incrementAndGet();
                                        listener.sessionDownload(i.get(),response.body().size());

                                        //Save data
                                        App.sqlite(Objects.requireNonNull(getActivity())).insertOrReplace(session);
                                        Log.d(TAG, SubjectSession.class.getSimpleName() + " - Saving...!");
                                    });
                                    Log.d(TAG, SubjectSession.class.getSimpleName() + " - Data saved!");
                                    return null;
                                }
                            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        } else {
                            listener.sessionDownload(1,1);
                            Log.d(TAG, SubjectSession.class.getSimpleName() + " - Response code: " + response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<List<SubjectSession>> call, Throwable t) {
                        listener.sessionDownload(1,1);
                        Log.d(TAG, SubjectSession.class.getSimpleName() + " - Cannot connect to the Server!");
                    }
                });

                //Get all slots
                API.build(getContext()).create(SubjectSlotController.class).getAllSlots(teacherCode).enqueue(new Callback<List<SubjectSlotDto>>() {
                    @Override
                    public void onResponse(Call<List<SubjectSlotDto>> call, Response<List<SubjectSlotDto>> response) {
                        if (response.code() == 200) {
                            new AsyncTask<Void, Void, Void>() {
                                @Override
                                protected Void doInBackground(Void... voids) {
                                    List<SubjectSlotDto> slotDtos = response.body();
                                    assert slotDtos != null;
                                    AtomicInteger i = new AtomicInteger(0);
                                    slotDtos.forEach(subjectSlotDto -> {
                                        i.incrementAndGet();
                                        listener.slotDownload(i.get(), slotDtos.size());

                                        SubjectSlot slot = new ModelMapper().map(subjectSlotDto, SubjectSlot.class);

                                        slot.setSubjectSemesterId(subjectSlotDto.getSubjectSemester().getId());
                                        slot.setStatusId(subjectSlotDto.getStatus().getId());
                                        slot.setSubjectId(subjectSlotDto.getSubjectSemester().getSubject().getId());
                                        slot.setTeacherId(subjectSlotDto.getSubjectSemester().getTeacher().getId());
                                        slot.setClazzId(subjectSlotDto.getSubjectSemester().getClazz().getId());
                                        slot.setRoomId(subjectSlotDto.getRoom().getId());
                                        AtomicReference<String> students = new AtomicReference<>("");
                                        subjectSlotDto.getSubjectSemester().getStudents().forEach(studentDto -> students.updateAndGet(v -> v + studentDto.getId() + ","));
                                        slot.setStudentList(students.get());

                                        App.sqlite(Objects.requireNonNull(getActivity())).insertOrReplace(slot);
                                    });
                                    Log.d(TAG, SubjectSlot.class.getSimpleName() + " - Data saved!");

                                    //Reload fragment
                                    onInit();
                                    return null;
                                }
                            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        } else {
                            listener.slotDownload(1,1);
                            Log.d(TAG, SubjectSlot.class.getSimpleName() + " - Response Code: " + response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<List<SubjectSlotDto>> call, Throwable t) {
                        listener.slotDownload(1,1);
                        Log.d(TAG, SubjectSlot.class.getSimpleName() + " - Cannot connect to the Server!");
                    }
                });

                //Get all SubjectSemester
                API.build(getContext()).create(TeacherController.class).getSubjectSemester(teacherCode).enqueue(new Callback<List<SubjectSemesterDto>>() {
                    @Override
                    public void onResponse(Call<List<SubjectSemesterDto>> call, Response<List<SubjectSemesterDto>> response) {
                        if (response.code() == 200) {
                            new AsyncTask<Void, Void, Void>() {
                                @Override
                                protected Void doInBackground(Void... voids) {
                                    //Class list to download data of Student
                                    Set<Long> classList = new HashSet<>();
                                    AtomicInteger i = new AtomicInteger();

                                    assert response.body() != null;
                                    ModelMapper mapper = new ModelMapper();
                                    response.body().forEach(ssDto -> {
                                        i.incrementAndGet();
                                        listener.subjectSemesterDownload(i.get(), response.body().size());

                                        classList.add(ssDto.getClazz().getId());

                                        SubjectSemester ss = mapper.map(ssDto, SubjectSemester.class);
                                        ss.setStatusId(ssDto.getStatus().getId());
                                        ss.setSemester(ssDto.getSemester().getDescription());
                                        ss.setSubjectId(ssDto.getSubject().getId());
                                        ss.setTeacherId(ssDto.getTeacher().getId());
                                        ss.setClazzId(ssDto.getClazz().getId());
                                        AtomicReference<String> listStudents = new AtomicReference<>("");
                                        ssDto.getStudents().forEach(studentDto -> listStudents.updateAndGet(v -> v + studentDto.getId() + ','));
                                        ss.setStudents(listStudents.get());
                                        //Save
                                        App.sqlite(Objects.requireNonNull(getActivity())).insertOrReplace(ss);
                                    });
                                    Log.d(TAG, SubjectSemester.class.getSimpleName() + " - Data saved!");

                                    //Use to download Student data
                                    classList.forEach(classId -> API.build(getContext()).create(StudentController.class).getStudentsByClass(classId).enqueue(new Callback<List<StudentDto>>() {
                                        @Override
                                        public void onResponse(Call<List<StudentDto>> call, Response<List<StudentDto>> response) {
                                            if (response.code() == 200) {
                                                new AsyncTask<Void, Void, Void>() {
                                                    @Override
                                                    protected Void doInBackground(Void... voids) {
                                                        assert response.body() != null;
                                                        ModelMapper modelMapper = new ModelMapper();
                                                        response.body().forEach(studentDto -> {
                                                            Student s = App.sqlite(Objects.requireNonNull(getActivity())).getStudentDao().load(studentDto.getId());
                                                            if (s == null) {
                                                                Student student = modelMapper.map(studentDto, Student.class);
                                                                student.setMajorId(studentDto.getMajor().getId());
                                                                student.setClazzId(studentDto.getClazz().getId());
                                                                App.sqlite(getActivity()).insertOrReplace(student);
                                                            }
                                                        });
                                                        Log.d(TAG, Student.class.getSimpleName() + " - Data saved!");
                                                        return null;
                                                    }
                                                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                            } else {
                                                Log.d(TAG, Student.class.getSimpleName() + " - Response code: " + response.code());
                                            }
                                            isStudentDownloaded = true;
                                        }

                                        @Override
                                        public void onFailure(Call<List<StudentDto>> call, Throwable t) {
                                            Log.d(TAG, Student.class.getSimpleName() + " - Cannot connect to Server!");
                                            isStudentDownloaded = true;
                                        }
                                    }));
                                    return null;
                                }
                            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            listener.subjectSemesterDownload(1,1);
                            Log.d(TAG, SubjectSemester.class.getSimpleName() + " - Response code: " + response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<List<SubjectSemesterDto>> call, Throwable t) {
                        listener.subjectSemesterDownload(1,1);
                        Log.d(TAG, SubjectSemester.class.getSimpleName() + " - Cannot connect to the Server!");
                    }
                });

                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


        //Waiting to download image of student
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                API.build(getContext()).create(HomeController.class).serverCheck().enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
                if (!App.isNetworkConnected(Objects.requireNonNull(getActivity()))) {
                    listener.studentDownload(1,1);
                    return null;
                }
                int d = 0;
                while (!isStudentDownloaded) {
                    ++d;
                    Log.d(TAG, Student.class.getSimpleName() + " - Waiting...");
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    //If data cannot download, destroy task
                    if (d==15) {
                        listener.studentDownload(1,1);
                        return null;
                    }
                }

                AtomicInteger i = new AtomicInteger();
                List<Student> list = App.sqlite(Objects.requireNonNull(getActivity())).getStudentDao().loadAll();
                list.forEach(student -> {
                    i.incrementAndGet();
                    listener.studentDownload(i.get(), list.size());

                    if (!student.getImage().contains("tech.tplus.akkoattendance")) {
                        Uri uri = App.downloadData(getActivity(), student.getImage(), "Students");
                        student.setImage(uri.toString());
                        App.sqlite(getActivity()).insertOrReplace(student);
                    }
                });
                Log.d(TAG, "Download all images of students");
                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}
