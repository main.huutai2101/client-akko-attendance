package tech.tplus.akkoattendance;

import android.app.Application;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import tech.tplus.akkoattendance.adapter.SlotAdapter;
import tech.tplus.akkoattendance.entity.*;
import tech.tplus.akkoattendance.helper.App;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class SlotFragment extends Fragment {
    private static final String TAG = SlotFragment.class.getSimpleName();

    private final SubjectSemester subjectSemester;


    public SlotFragment(SubjectSemester subjectSemester) {
        this.subjectSemester = subjectSemester;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_slot, container, false);

        //Get list Slots
        List<Student> studentList = App.sqlite(Objects.requireNonNull(getActivity())).getStudentDao().loadAll();
        List<Room> roomList = App.sqlite(Objects.requireNonNull(getActivity())).getRoomDao().loadAll();
        List<SubjectSlot> slotList = App.sqlite(Objects.requireNonNull(getActivity())).getSubjectSlotDao().loadAll();
        slotList = slotList.stream().filter(subjectSlot -> subjectSlot.getSubjectSemesterId().equals(subjectSemester.getId())).collect(Collectors.toList());
        SlotAdapter adapter = new SlotAdapter(slotList, roomList);

        //Get Class
        Clazz clazz = App.sqlite(getActivity()).getClazzDao().load(subjectSemester.getClazzId());
        //Get Subject
        Subject subject = App.sqlite(getActivity()).getSubjectDao().load(subjectSemester.getSubjectId());

        TextView txtTitle = view.findViewById(R.id.txtTitle);
        TextView txtDescription = view.findViewById(R.id.txtDescription);
        ListView lvSlot = view.findViewById(R.id.lvSlot);
        TextView txtCount = view.findViewById(R.id.txtCount);

        txtTitle.setText(clazz.getCode());
        txtDescription.setText(subject.getCode() + " - " + subject.getName());
        lvSlot.setAdapter(adapter);
        App.setListViewHeightBasedOnChildren(lvSlot);


        txtCount.setText(getResources().getString(R.string.total) + ' ' + slotList.size() + ' ' + getResources().getString(R.string.slot_count));

        lvSlot.setOnItemClickListener((parent, view1, position, id) -> {
            assert getFragmentManager() != null;
            getFragmentManager().beginTransaction()
                    .add(R.id.home_container, new StudentFragment((SubjectSlot)parent.getItemAtPosition(position)))
                    .addToBackStack(TAG)
                    .commit();
        });

        return view;
    }

}
