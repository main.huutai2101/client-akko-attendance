package tech.tplus.akkoattendance;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import tech.tplus.akkoattendance.adapter.ClazzAdapter;
import tech.tplus.akkoattendance.adapter.SubjectAdapter;
import tech.tplus.akkoattendance.entity.Clazz;
import tech.tplus.akkoattendance.entity.Student;
import tech.tplus.akkoattendance.entity.Subject;
import tech.tplus.akkoattendance.helper.App;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


public class ClassFragment extends Fragment {
    private static final String TAG = ClassFragment.class.getSimpleName();

    private ClassFragListener listener;
    private List<Clazz> clazzList;

    public ClassFragment() {
        clazzList = null;
    }

    public ClassFragment(List<Clazz> clazzList) {
        this.clazzList = clazzList;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_class, container, false);

        if (clazzList == null) clazzList = App.sqlite(Objects.requireNonNull(getActivity())).getClazzDao().loadAll();

        ImageView icBack = view.findViewById(R.id.img_back);
        icBack.setOnClickListener(v -> listener.classFrag(true));

        ClazzAdapter adapter = new ClazzAdapter(clazzList);

        TextView txtCount = view.findViewById(R.id.txtCount);
        txtCount.setText(getResources().getString(R.string.total) + ' ' + clazzList.size() + ' ' + getResources().getString(R.string.class_count));

        ListView lvSubject = view.findViewById(R.id.lvSubject);
        lvSubject.setAdapter(adapter);
        App.setListViewHeightBasedOnChildren(lvSubject);
        lvSubject.setOnItemClickListener((parent, view1, position, id) -> {
            Clazz clazz = (Clazz) parent.getItemAtPosition(position);
            List<Student> studentList = App.sqlite(Objects.requireNonNull(getActivity())).getStudentDao().loadAll().stream().filter(student -> student.getClazzId().equals(clazz.getId())).collect(Collectors.toList());
            assert getFragmentManager() != null;
            getFragmentManager().beginTransaction()
                    .add(R.id.home_container, new StudentFragment(clazz, studentList))
                    .addToBackStack(TAG)
                    .commit();
        });
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ClassFragment.ClassFragListener) {
            listener = (ClassFragment.ClassFragListener) context;
        } else {
            Log.d(TAG, "Cannot attach data to HomeActivity");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public interface ClassFragListener {
        void classFrag(boolean isBackPress);
    }
}
