package tech.tplus.akkoattendance;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import tech.tplus.akkoattendance.adapter.SubjectAdapter;
import tech.tplus.akkoattendance.entity.Clazz;
import tech.tplus.akkoattendance.entity.Subject;
import tech.tplus.akkoattendance.entity.SubjectSemester;
import tech.tplus.akkoattendance.helper.App;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


public class SubjectFragment extends Fragment {
    private static final String TAG = SubjectFragment.class.getSimpleName();

    private SubjectFragListener listener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_subject, container, false);

        ImageView icBack = view.findViewById(R.id.img_back);
        icBack.setOnClickListener(v -> listener.subjectFrag(true));


        List<Subject> subjectList = App.sqlite(Objects.requireNonNull(getActivity())).getSubjectDao().loadAll();
        SubjectAdapter adapter = new SubjectAdapter(subjectList);

        TextView txtCount = view.findViewById(R.id.txtCount);
        txtCount.setText(getResources().getString(R.string.total) + ' ' + subjectList.size() + ' ' + getResources().getString(R.string.subject_count));

        ListView lvSubject = view.findViewById(R.id.lvSubject);
        lvSubject.setAdapter(adapter);
        App.setListViewHeightBasedOnChildren(lvSubject);
        lvSubject.setOnItemClickListener((parent, view1, position, id) -> {
            //Get subject item
            Subject subject = (Subject) parent.getItemAtPosition(position);

            //Get subject semester by subject
            List<SubjectSemester> ssList = App.sqlite(getActivity()).getSubjectSemesterDao().loadAll().stream().filter(ss -> ss.getSubjectId().equals(subject.getId())).collect(Collectors.toList());

            //Get all class learning this subject
            List<Clazz> clazzList = new ArrayList<>();
            for(SubjectSemester ss: ssList) {
                clazzList.add(App.sqlite(getActivity()).getClazzDao().load(ss.getClazzId()));
            }

            //Open new frag with class list
            assert getFragmentManager() != null;
            getFragmentManager().beginTransaction()
                    .add(R.id.home_container, new ClassFragment(clazzList))
                    .addToBackStack(TAG)
                    .commit();
        });
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof SubjectFragment.SubjectFragListener) {
            listener = (SubjectFragment.SubjectFragListener) context;
        } else {
            Log.d(TAG, "Cannot attach data to HomeActivity");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public interface SubjectFragListener {
        void subjectFrag(boolean isBackPress);
    }
}
