package tech.tplus.akkoattendance;

import android.os.Bundle;
import android.widget.EditText;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import tech.tplus.akkoattendance.entity.Subject;


public class SubjectDetailsFragment extends Fragment {

    private final Subject subject;

    public SubjectDetailsFragment(Subject subject) {
        this.subject = subject;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_subject_details, container, false);

        EditText edSubjectCode = view.findViewById(R.id.edSubjectCode);
        EditText edSubjectName = view.findViewById(R.id.edSubjectName);
        EditText edSubjectDescription = view.findViewById(R.id.edSubjectDescription);

        edSubjectCode.setText(subject.getCode());
        edSubjectName.setText(subject.getName());
        edSubjectDescription.setText(subject.getDescription());
        
        return view;
    }
}
