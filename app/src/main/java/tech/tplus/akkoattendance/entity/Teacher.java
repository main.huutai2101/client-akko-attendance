package tech.tplus.akkoattendance.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;

import org.greenrobot.greendao.annotation.Generated;

@Entity
public class Teacher {
    @Id
    private Long id;
    private String code;
    private String fullName;
    private String birthday;
    private int gender;
    private String address;
    private String phone;
    private String email;
    private String image;
    private String entryYear;
    private String experience;
    private String coverImage;
    @Generated(hash = 701753446)
    public Teacher(Long id, String code, String fullName, String birthday,
            int gender, String address, String phone, String email, String image,
            String entryYear, String experience, String coverImage) {
        this.id = id;
        this.code = code;
        this.fullName = fullName;
        this.birthday = birthday;
        this.gender = gender;
        this.address = address;
        this.phone = phone;
        this.email = email;
        this.image = image;
        this.entryYear = entryYear;
        this.experience = experience;
        this.coverImage = coverImage;
    }
    @Generated(hash = 1630413260)
    public Teacher() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getCode() {
        return this.code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getFullName() {
        return this.fullName;
    }
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
    public String getBirthday() {
        return this.birthday;
    }
    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }
    public int getGender() {
        return this.gender;
    }
    public void setGender(int gender) {
        this.gender = gender;
    }
    public String getAddress() {
        return this.address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getPhone() {
        return this.phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getEmail() {
        return this.email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getImage() {
        return this.image;
    }
    public void setImage(String image) {
        this.image = image;
    }
    public String getEntryYear() {
        return this.entryYear;
    }
    public void setEntryYear(String entryYear) {
        this.entryYear = entryYear;
    }
    public String getExperience() {
        return this.experience;
    }
    public void setExperience(String experience) {
        this.experience = experience;
    }
    public String getCoverImage() {
        return this.coverImage;
    }
    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    
}
