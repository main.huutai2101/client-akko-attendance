package tech.tplus.akkoattendance.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class Student {
    @Id
    private Long id;
    private String code;
    private String fullName;
    private String birthday;
    private int gender;
    private String address;
    private String phone;
    private String email;
    private String image;

    private Long clazzId;
    private Long majorId;
    @Generated(hash = 883636789)
    public Student(Long id, String code, String fullName, String birthday,
            int gender, String address, String phone, String email, String image,
            Long clazzId, Long majorId) {
        this.id = id;
        this.code = code;
        this.fullName = fullName;
        this.birthday = birthday;
        this.gender = gender;
        this.address = address;
        this.phone = phone;
        this.email = email;
        this.image = image;
        this.clazzId = clazzId;
        this.majorId = majorId;
    }
    @Generated(hash = 1556870573)
    public Student() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getCode() {
        return this.code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getFullName() {
        return this.fullName;
    }
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
    public String getBirthday() {
        return this.birthday;
    }
    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }
    public int getGender() {
        return this.gender;
    }
    public void setGender(int gender) {
        this.gender = gender;
    }
    public String getAddress() {
        return this.address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getPhone() {
        return this.phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getEmail() {
        return this.email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getImage() {
        return this.image;
    }
    public void setImage(String image) {
        this.image = image;
    }
    public Long getClazzId() {
        return this.clazzId;
    }
    public void setClazzId(Long clazzId) {
        this.clazzId = clazzId;
    }
    public Long getMajorId() {
        return this.majorId;
    }
    public void setMajorId(Long majorId) {
        this.majorId = majorId;
    }
}
