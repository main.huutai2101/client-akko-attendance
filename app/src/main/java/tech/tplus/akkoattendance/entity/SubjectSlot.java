package tech.tplus.akkoattendance.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;

import java.util.Date;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class SubjectSlot {
    @Id
    private Long id;
    private Integer slotNumber;
    private Date date;

    private Long statusId;
    private Long subjectSemesterId;
    private Long subjectId;
    private Long teacherId;
    private Long clazzId;
    private Long roomId;
    private String studentList;
    @Generated(hash = 1550944265)
    public SubjectSlot(Long id, Integer slotNumber, Date date, Long statusId,
            Long subjectSemesterId, Long subjectId, Long teacherId, Long clazzId,
            Long roomId, String studentList) {
        this.id = id;
        this.slotNumber = slotNumber;
        this.date = date;
        this.statusId = statusId;
        this.subjectSemesterId = subjectSemesterId;
        this.subjectId = subjectId;
        this.teacherId = teacherId;
        this.clazzId = clazzId;
        this.roomId = roomId;
        this.studentList = studentList;
    }
    @Generated(hash = 848881889)
    public SubjectSlot() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Integer getSlotNumber() {
        return this.slotNumber;
    }
    public void setSlotNumber(Integer slotNumber) {
        this.slotNumber = slotNumber;
    }
    public Date getDate() {
        return this.date;
    }
    public void setDate(Date date) {
        this.date = date;
    }
    public Long getStatusId() {
        return this.statusId;
    }
    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }
    public Long getSubjectSemesterId() {
        return this.subjectSemesterId;
    }
    public void setSubjectSemesterId(Long subjectSemesterId) {
        this.subjectSemesterId = subjectSemesterId;
    }
    public Long getSubjectId() {
        return this.subjectId;
    }
    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }
    public Long getTeacherId() {
        return this.teacherId;
    }
    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }
    public Long getClazzId() {
        return this.clazzId;
    }
    public void setClazzId(Long clazzId) {
        this.clazzId = clazzId;
    }
    public Long getRoomId() {
        return this.roomId;
    }
    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }
    public String getStudentList() {
        return this.studentList;
    }
    public void setStudentList(String studentList) {
        this.studentList = studentList;
    }
    
}
