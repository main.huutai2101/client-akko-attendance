package tech.tplus.akkoattendance.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class SubjectSession {
    @Id
    private Long id;
    private Long statusId;
    private Long studentId;
    private Long slotId;
    @Generated(hash = 1327421399)
    public SubjectSession(Long id, Long statusId, Long studentId, Long slotId) {
        this.id = id;
        this.statusId = statusId;
        this.studentId = studentId;
        this.slotId = slotId;
    }
    @Generated(hash = 1755301083)
    public SubjectSession() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Long getStatusId() {
        return this.statusId;
    }
    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }
    public Long getStudentId() {
        return this.studentId;
    }
    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }
    public Long getSlotId() {
        return this.slotId;
    }
    public void setSlotId(Long slotId) {
        this.slotId = slotId;
    }
}
