package tech.tplus.akkoattendance.entity;


import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class Room {
    @Id
    private Long id;
    private String code;
    private String floor;
    private Integer capacity;
    @Generated(hash = 704656932)
    public Room(Long id, String code, String floor, Integer capacity) {
        this.id = id;
        this.code = code;
        this.floor = floor;
        this.capacity = capacity;
    }
    @Generated(hash = 703125385)
    public Room() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getCode() {
        return this.code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getFloor() {
        return this.floor;
    }
    public void setFloor(String floor) {
        this.floor = floor;
    }
    public Integer getCapacity() {
        return this.capacity;
    }
    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }
}
