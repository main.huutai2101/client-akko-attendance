package tech.tplus.akkoattendance.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class MapEntity {
    @Id
    private String key;
    private String value;
    @Generated(hash = 1424259521)
    public MapEntity(String key, String value) {
        this.key = key;
        this.value = value;
    }
    @Generated(hash = 717763387)
    public MapEntity() {
    }
    public String getKey() {
        return this.key;
    }
    public void setKey(String key) {
        this.key = key;
    }
    public String getValue() {
        return this.value;
    }
    public void setValue(String value) {
        this.value = value;
    }
}
