package tech.tplus.akkoattendance.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;

import java.util.Date;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class SubjectSemester {
    @Id
    private Long id;
    private Integer numberOfSlots;
    private String advisor;
    private Date startDate;

    private Long statusId;
    private String semester;
    private Long subjectId;
    private Long teacherId;
    private Long clazzId;
    private String students; //List students
    @Generated(hash = 679796139)
    public SubjectSemester(Long id, Integer numberOfSlots, String advisor,
            Date startDate, Long statusId, String semester, Long subjectId,
            Long teacherId, Long clazzId, String students) {
        this.id = id;
        this.numberOfSlots = numberOfSlots;
        this.advisor = advisor;
        this.startDate = startDate;
        this.statusId = statusId;
        this.semester = semester;
        this.subjectId = subjectId;
        this.teacherId = teacherId;
        this.clazzId = clazzId;
        this.students = students;
    }
    @Generated(hash = 302713563)
    public SubjectSemester() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Integer getNumberOfSlots() {
        return this.numberOfSlots;
    }
    public void setNumberOfSlots(Integer numberOfSlots) {
        this.numberOfSlots = numberOfSlots;
    }
    public String getAdvisor() {
        return this.advisor;
    }
    public void setAdvisor(String advisor) {
        this.advisor = advisor;
    }
    public Date getStartDate() {
        return this.startDate;
    }
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    public Long getStatusId() {
        return this.statusId;
    }
    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }
    public String getSemester() {
        return this.semester;
    }
    public void setSemester(String semester) {
        this.semester = semester;
    }
    public Long getSubjectId() {
        return this.subjectId;
    }
    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }
    public Long getTeacherId() {
        return this.teacherId;
    }
    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }
    public Long getClazzId() {
        return this.clazzId;
    }
    public void setClazzId(Long clazzId) {
        this.clazzId = clazzId;
    }
    public String getStudents() {
        return this.students;
    }
    public void setStudents(String students) {
        this.students = students;
    }
}
