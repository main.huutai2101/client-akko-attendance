package tech.tplus.akkoattendance.entity;


import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class Clazz {
    @Id
    private Long id;
    private String code;
    private String description;
    @Generated(hash = 944091962)
    public Clazz(Long id, String code, String description) {
        this.id = id;
        this.code = code;
        this.description = description;
    }
    @Generated(hash = 1166360579)
    public Clazz() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getCode() {
        return this.code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getDescription() {
        return this.description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

}
