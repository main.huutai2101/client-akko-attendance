package tech.tplus.akkoattendance;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import tech.tplus.akkoattendance.adapter.ClazzAdapter;
import tech.tplus.akkoattendance.adapter.SubjectSemesterAdapter;
import tech.tplus.akkoattendance.entity.Clazz;
import tech.tplus.akkoattendance.entity.Subject;
import tech.tplus.akkoattendance.entity.SubjectSemester;
import tech.tplus.akkoattendance.entity.SubjectSlot;
import tech.tplus.akkoattendance.helper.App;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


public class AttendanceFragment extends Fragment {
    private static final String TAG = AttendanceFragment.class.getSimpleName();

    private AttendanceFragListener listener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_attendance, container, false);

        ImageView icBack = view.findViewById(R.id.img_back);
        icBack.setOnClickListener(v -> listener.attendanceFrag(true));

        List<SubjectSemester> ssList = App.sqlite(Objects.requireNonNull(getActivity())).getSubjectSemesterDao().loadAll();
        List<Subject> subjectList = App.sqlite(Objects.requireNonNull(getActivity())).getSubjectDao().loadAll();
        List<Clazz> clazzList = App.sqlite(Objects.requireNonNull(getActivity())).getClazzDao().loadAll();
        SubjectSemesterAdapter adapter = new SubjectSemesterAdapter(ssList, subjectList, clazzList);

        TextView txtCount = view.findViewById(R.id.txtCount);
        txtCount.setText(getResources().getString(R.string.total) + ' ' + ssList.size() + ' ' + getResources().getString(R.string.course_count));

        ListView lvSubject = view.findViewById(R.id.lvSubject);
        lvSubject.setAdapter(adapter);
        App.setListViewHeightBasedOnChildren(lvSubject);
        lvSubject.setOnItemClickListener((parent, view1, position, id) -> {
            SubjectSemester ss = (SubjectSemester) parent.getItemAtPosition(position);
            assert getFragmentManager() != null;
            getFragmentManager().beginTransaction()
                    .add(R.id.home_container, new SlotFragment(ss))
                    .addToBackStack(TAG)
                    .commit();
        });
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof AttendanceFragment.AttendanceFragListener) {
            listener = (AttendanceFragment.AttendanceFragListener) context;
        } else {
            Log.d(TAG, "Cannot attach data to HomeActivity");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public interface AttendanceFragListener {
        void attendanceFrag(boolean isBackPress);
    }
}
