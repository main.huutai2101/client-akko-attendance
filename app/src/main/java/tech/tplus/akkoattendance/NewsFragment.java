package tech.tplus.akkoattendance;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.AbsListView;
import android.widget.ListView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tech.tplus.akkoattendance.adapter.NewsAdapter;
import tech.tplus.akkoattendance.controller.NewsController;
import tech.tplus.akkoattendance.entity.News;
import tech.tplus.akkoattendance.helper.API;
import tech.tplus.akkoattendance.helper.App;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;


public class NewsFragment extends Fragment {
    private static final String TAG = NewsFragment.class.getSimpleName();
    private static final int NEWS_LIMIT = 10;

    View view;
    ListView listView;

    private NewsFragListener listener;
    private boolean scrollUpdate = false;
    private boolean loadAllNews = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        listener.newsDownload(0, 1);
        if (view == null) view = inflater.inflate(R.layout.fragment_news, container, false);
        if (App.isNetworkConnected(Objects.requireNonNull(getActivity()))) doSynchronized();
        else listener.newsDownload(1,1);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        onFind();
        onInit();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof NewsFragment.NewsFragListener) {
            listener = (NewsFragment.NewsFragListener) context;
        } else {
            Log.d(TAG, "Cannot attach data to HomeActivity");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    /**
     * Find Id for View
     */
    private void onFind() {
        listView = view.findViewById(R.id.lv_news);
    }

    /**
     * Synchronized in background, update News
     */
    @SuppressLint("StaticFieldLeak")
    private void doSynchronized() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                //Create API to call
                NewsController newsController =  API.build(getContext()).create(NewsController.class);
                Call<List<News>> call = newsController.latestNewsWithLimit(NEWS_LIMIT);

                //Check list
                List<News> newsList = App.sqlite(Objects.requireNonNull(getActivity())).getNewsDao().loadAll();
                if (!newsList.isEmpty()) {
                    int lastIndex = newsList.size() - 1;
                    call = newsController.updateNews(newsList.get(lastIndex).getId());
                }

                //Get data from Server
                call.enqueue(new Callback<List<News>>() {
                    @Override
                    public void onResponse(Call<List<News>> call, Response<List<News>> response) {
                        if (response.code() == 200) {
                            new AsyncTask<Void, Void, Void>() {
                                @Override
                                protected Void doInBackground(Void... voids) {
                                    //Get list new and set unread
                                    List<News> resNewsList = response.body();
                                    assert resNewsList != null;
                                    Log.d(TAG, resNewsList.size() + " news is updated!");
                                    resNewsList.forEach(news -> news.setIsRead(false));

                                    //Get old delete, and set limit as NEWS_LIMIT
                                    List<News> oldList = App.sqlite(getActivity()).getNewsDao().loadAll();
                                    oldList.addAll(resNewsList);
                                    int sizeList = oldList.size();
                                    oldList.subList(Math.max(sizeList - NEWS_LIMIT, 0), sizeList);

                                    //Delete old database
                                    App.sqlite(getActivity()).getNewsDao().deleteAll();

                                    //Add new
                                    AtomicInteger i = new AtomicInteger(0);
                                    oldList.forEach(news -> {
                                        i.incrementAndGet();
                                        listener.newsDownload(i.get(), oldList.size());
                                        App.sqlite(getActivity()).insertOrReplace(news);
                                    });

                                    onInit();

                                    Log.d(TAG, "=== End Synchronization ===");
                                    return null;
                                }
                            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            Log.d(TAG, "Response code: " + response.code());
                            Log.d(TAG, "=== End Synchronization ===");
                            listener.newsDownload(1,1);
                        }
                    }

                    @Override
                    public void onFailure(Call<List<News>> call, Throwable t) {
                        Log.d(TAG, "Unable to connect to the Server!");
                        Log.d(TAG, "=== End Synchronization ===");
                        listener.newsDownload(1,1);
                    }
                });
                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    /**
     * Init data to View
     */
    private void onInit() {
        loadAllNews = false;
        scrollUpdate = false;

        //Get data from SQLite
        List<News> newsList = App.sqlite(Objects.requireNonNull(getActivity())).getNewsDao().loadAll();
        Collections.reverse(newsList);

        NewsAdapter newsAdapter = new NewsAdapter(Objects.requireNonNull(getContext()), newsList);
        listView.post(() -> listView.setAdapter(newsAdapter));
        listView.setOnItemClickListener((parent, view, position, id) -> {
            News news = (News) parent.getItemAtPosition(position);


            if (!news.getIsRead()) {
                //Set news is read and change view
                news.setIsRead(true);
                App.sqlite(Objects.requireNonNull(getActivity())).insertOrReplace(news);
                newsAdapter.notifyDataSetChanged();
                //Change number of unread news on News icon in HomeActivity
                listener.newsFrag(numberOfUnreadNews());
            }


            //Open news details
            assert getFragmentManager() != null;
            getFragmentManager().beginTransaction()
                    .add(R.id.news_container, new NewsDetailsFragment(news))
                    .addToBackStack(TAG)
                    .commit();
        });
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                int lastPosition = listView.getLastVisiblePosition();
                int size = listView.getCount();
                if (lastPosition == size - 1 && !scrollUpdate && !loadAllNews) {
                    scrollUpdate = true;

                    //Request to Server to Update News
                    News news = (News) listView.getItemAtPosition(lastPosition);
                    Log.d(TAG, "Last position of the list view! - News Id: " + news.getId());

                    API.build(getContext())
                            .create(NewsController.class)
                            .scrollUpdate(news.getId(), NEWS_LIMIT)
                            .enqueue(new Callback<List<News>>() {
                                @Override
                                public void onResponse(Call<List<News>> call, Response<List<News>> response) {
                                    if (response.code() == 200) {
                                        List<News> resNewsList = response.body();
                                        assert resNewsList != null;
                                        if (resNewsList.size() == 0) loadAllNews = true;
                                        Log.d(TAG, resNewsList.size() + " news is updated!");
                                        resNewsList.forEach(n -> n.setIsRead(true));
                                        NewsAdapter adapter = (NewsAdapter) listView.getAdapter();
                                        adapter.addItem(resNewsList);
                                        adapter.notifyDataSetChanged();
                                    }
                                    scrollUpdate = false;
                                }

                                @Override
                                public void onFailure(Call<List<News>> call, Throwable t) {
                                    scrollUpdate = false;
                                }
                    });

                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });
        listener.newsFrag(numberOfUnreadNews());
    }

    /**
     * Get number of unread
     * @return number of unread news
     */
    private int numberOfUnreadNews() {
        List<News> list = App.sqlite(Objects.requireNonNull(getActivity())).getNewsDao().loadAll();
        return (int) list.stream().filter(news -> !news.getIsRead()).count();
    }

    public interface NewsFragListener {
        void newsFrag(int unreadNews);
        void newsDownload(int process, int size);
    }
}
