package tech.tplus.akkoattendance;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tech.tplus.akkoattendance.adapter.StudentAdapter;
import tech.tplus.akkoattendance.controller.SessionController;
import tech.tplus.akkoattendance.dto.AttendanceDto;
import tech.tplus.akkoattendance.entity.*;
import tech.tplus.akkoattendance.helper.API;
import tech.tplus.akkoattendance.helper.App;
import tech.tplus.akkoattendance.other.PopupResult;
import tech.tplus.akkoattendance.other.PopupStatus;
import tech.tplus.akkoattendance.other.PopupType;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class StudentFragment extends Fragment {

    private static final String TAG = StudentFragment.class.getSimpleName();
    private static final int SLOT = 0;
    private static final int STUDENT = 1;

    private final List<Student> students;
    private final Clazz clazz;
    private SubjectSlot slot;
    private final int type;

    private StudentFragListener listener;

    public StudentFragment(SubjectSlot slot) {
        this.clazz = null;
        this.slot = slot;
        this.students = new ArrayList<>();
        this.type = SLOT;
    }

    public StudentFragment(Clazz clazz, List<Student> students) {
        this.clazz = clazz;
        this.students = students;
        this.type = STUDENT;
    }

    private int getSlot(Date date) {
        final int[] hours =   {7,  8,  10, 12, 14, 16, 18, 20};
        final int[] minutes = {0, 45,  30, 45, 30, 15, 30, 15};
        for(int i=0; i<hours.length; ++i)
            if (date.getHours() == hours[i] && date.getMinutes() == minutes[i]) return i+1;
        return -1;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_student, container, false);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof StudentFragment.StudentFragListener) {
            listener = (StudentFragment.StudentFragListener) context;
        } else {
            Log.d(TAG, "Cannot attach data to HomeActivity");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView txtNotYet = view.findViewById(R.id.not_yet);
        TextView txtAttended = view.findViewById(R.id.attended);
        TextView txtAbsent = view.findViewById(R.id.absent);

        TextView txtTitle = view.findViewById(R.id.txt_title);
        TextView txtDesc = view.findViewById(R.id.txt_description);
        TextView txtCount = view.findViewById(R.id.txtCount);
        ListView lvStudent = view.findViewById(R.id.lv_student);
        ImageView imgBack = view.findViewById(R.id.img_back);
        LinearLayout layout = view.findViewById(R.id.row_count);
        imgBack.setOnClickListener(v -> listener.studentFrag(true));

        if (type == SLOT) {
            List<SubjectSession> sessions = App.sqlite(Objects.requireNonNull(getActivity())).getSubjectSessionDao().loadAll();
            sessions = sessions.stream().filter(session -> session.getSlotId().equals(slot.getId())).collect(Collectors.toList());
            sessions.sort(Comparator.comparing(SubjectSession::getStudentId));


            int notYet = 0;
            int attended = 0;
            int absent = 0;
            for(SubjectSession s: sessions) {
                if (s.getStatusId() == 3) ++attended;
                else if (s.getStatusId() == 4) ++absent;
                else if (s.getStatusId() == 5) ++ notYet;
            }

            Clazz clazz = App.sqlite(getActivity()).getClazzDao().load(slot.getClazzId());
            Subject subject = App.sqlite(getActivity()).getSubjectDao().load(slot.getSubjectId());
            SubjectSemester subjectSemester = App.sqlite(getActivity()).getSubjectSemesterDao().load(slot.getSubjectSemesterId());
            String[] ids = subjectSemester.getStudents().split(",");
            Arrays.asList(ids).forEach(id -> students.add(App.sqlite(getActivity()).getStudentDao().load(new Long(id))));
            students.sort(Comparator.comparing(Student::getId));

            layout.setVisibility(View.VISIBLE);
            txtNotYet.setText(getResources().getString(R.string.not_yet_2) + '\n' + notYet + '/' + sessions.size());
            txtAttended.setText(getResources().getString(R.string.attended) + '\n' + attended + '/' + sessions.size());
            txtAbsent.setText(getResources().getString(R.string.absent) + '\n' + absent + '/' + sessions.size());


            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            StudentAdapter adapter = new StudentAdapter(students, sessions, false);

            txtTitle.post(() -> txtTitle.setText(clazz.getCode() + " - " + subject.getCode()));
            txtDesc.post(() -> txtDesc.setText(view.getResources().getString(R.string.slot) + " " + getSlot(slot.getDate()) + " - " + sdf.format(slot.getDate())));

            lvStudent.setAdapter(adapter);
            App.setListViewHeightBasedOnChildren(lvStudent);

            LocalDateTime today = LocalDateTime.now();
            LocalDateTime slotDate = slot.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            long days =  Duration.between(today.toLocalDate().atStartOfDay(), slotDate.toLocalDate().atStartOfDay()).toDays();

            if (days >=-2 && today.isAfter(slotDate)) {
                LinearLayout take = view.findViewById(R.id.take);
                LinearLayout btnByHand = view.findViewById(R.id.by_hand);
                LinearLayout btnByCamera = view.findViewById(R.id.by_camera);

                LinearLayout submitReset = view.findViewById(R.id.submit_reset);
                TextView txtSubmit = view.findViewById(R.id.txt_submit);
                TextView txtReset = view.findViewById(R.id.txt_reset);

                take.setVisibility(View.VISIBLE);
                List<SubjectSession> finalSessions = new ArrayList<>(sessions);
                List<Long> listStatus = new ArrayList<>();
                finalSessions.forEach(ss -> {
                    if (ss.getStatusId().equals(5L)) listStatus.add(4L);
                    else listStatus.add(new Long(ss.getStatusId().toString()));
                });

                btnByHand.setOnClickListener(v -> {
                    //Hide take attendance actions
                    take.setVisibility(View.GONE);

                    //Hide count
                    layout.setVisibility(View.GONE);

                    //Set data into view
                    submitReset.setVisibility(View.VISIBLE);

                    for (int i = 0; i<listStatus.size(); ++i) {
                        finalSessions.get(i).setStatusId(listStatus.get(i));
                    }

                    StudentAdapter takeAdapter = new StudentAdapter(students, finalSessions, true);
                    lvStudent.setAdapter(takeAdapter);
                    App.setListViewHeightBasedOnChildren(lvStudent);
                    adapter.notifyDataSetChanged();

                    txtReset.setOnClickListener(v1 -> new Popup(Objects.requireNonNull(getContext()), getString(R.string.warning_title), getString(R.string.reset_msg), PopupStatus.WARNING, PopupType.YES_NO)
                            .setPopupListener(result -> {
                                if (result == PopupResult.YES_OK) {
                                    for (int i = 0; i<listStatus.size(); ++i) finalSessions.get(i).setStatusId(listStatus.get(i));
                                    StudentAdapter newAdapter = new StudentAdapter(students, finalSessions, true);
                                    lvStudent.setAdapter(newAdapter);
                                    App.setListViewHeightBasedOnChildren(lvStudent);
                                    adapter.notifyDataSetChanged();
                                }
                            }).show());

                    //Submit data
                    txtSubmit.setOnClickListener(v1 -> submit(finalSessions));

                });
                btnByCamera.setOnClickListener(v -> new Popup(Objects.requireNonNull(getContext()), getString(R.string.coming_soon), getString(R.string.coming_content), PopupStatus.WARNING, PopupType.OK).show());
            }


        } else if (type == STUDENT) {
            assert clazz != null;
            txtTitle.setText(clazz.getCode() + ' ' + getResources().getString(R.string.group));
            txtDesc.setText(getResources().getString(R.string.group_description));
            txtCount.setText(getResources().getString(R.string.total) + ' ' + students.size() + ' ' + getResources().getString(R.string.student_count));
            txtCount.setVisibility(View.VISIBLE);

            StudentAdapter adapter = new StudentAdapter(students);
            lvStudent.setAdapter(adapter);
            App.setListViewHeightBasedOnChildren(lvStudent);
        }
    }

    private void submit(List<SubjectSession> sessionList) {
        API.build(getContext()).create(SessionController.class).post(new AttendanceDto(slot.getId(), sessionList)).enqueue(new Callback<AttendanceDto>() {
            @Override
            public void onResponse(Call<AttendanceDto> call, Response<AttendanceDto> response) {
                if (response.code() == 200) {
                    new Popup(Objects.requireNonNull(getContext()), getString(R.string.success_title), getString(R.string.attended_success), PopupStatus.SUCCESS, PopupType.OK)
                            .setPopupListener(result -> {
                                if (result == PopupResult.OK) goHome();
                            })
                            .show();
                    slot.setStatusId(7L);
                    App.sqlite(Objects.requireNonNull(getActivity())).insertOrReplace(slot);
                    sessionList.forEach(session -> App.sqlite(getActivity()).insertOrReplace(session));
                } else {
                    new Popup(Objects.requireNonNull(getContext()), getString(R.string.error_title), getString(R.string.attended_failed) + "\nCode: " + response.code(), PopupStatus.DANGER, PopupType.OK).show();
                }
            }

            @Override
            public void onFailure(Call<AttendanceDto> call, Throwable t) {
                new Popup(Objects.requireNonNull(getContext()), getString(R.string.warning_title), getString(R.string.something_wrong), PopupStatus.WARNING, PopupType.OK).show();
            }
        });
    }

    private void goHome() {
        assert getFragmentManager() != null;

        FragmentManager fm = getFragmentManager();

        int i = fm.getFragments().size()-2;

        Fragment f = fm.getFragments().get(i);
        int d = 1;
        if (f instanceof TimetableFragment) d = 2;
        else if (f instanceof SlotFragment) d = 3;

        for(i = 0; i<d; ++i) fm.popBackStack();

        //Reload home frag
        FragmentTransaction ft = fm.beginTransaction();
        if (Build.VERSION.SDK_INT >= 26) ft.setReorderingAllowed(false);


        for(Fragment fragment: fm.getFragments()) {
            if (fragment instanceof HomeFragment) {
                ft.detach(fragment).attach(fragment).commit();
                break;
            }
        }

    }

    public interface StudentFragListener {
        void studentFrag(boolean isBack);
    }
}
