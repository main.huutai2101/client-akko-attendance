package tech.tplus.akkoattendance;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.thekhaeng.pushdownanim.PushDownAnim;
import lombok.SneakyThrows;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.Retrofit.Builder;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import tech.tplus.akkoattendance.controller.GGAuthController;
import tech.tplus.akkoattendance.dto.GGAuthDto;
import tech.tplus.akkoattendance.dto.TeacherDto;
import tech.tplus.akkoattendance.entity.MapEntity;
import tech.tplus.akkoattendance.helper.API;
import tech.tplus.akkoattendance.helper.App;
import tech.tplus.akkoattendance.other.PopupStatus;
import tech.tplus.akkoattendance.other.PopupType;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class LoginActivity extends AppCompatActivity  implements GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = LoginActivity.class.getSimpleName();
    private static final int SIGN_IN_CODE = 56446;

    private GoogleApiClient googleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        App.checkPermission(this);
        if (isLogin()) gotoHome();

        googleInit();

        ImageView btnLogin = findViewById(R.id.btn_login);
        PushDownAnim.setPushDownAnimTo(btnLogin).setOnClickListener(v -> login());

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SIGN_IN_CODE) {
            loginResult(data);
        }
    }


    /**
     * While list for login (must be create in Server)
     * @param email email to check
     * @return while list or black list
     */
    private boolean checkAuth(String email) {
        List<String> whileList = Arrays.asList(
                "akko.khanhvh@gmail.com",
                "akko.dalq@gmail.com",
                "akko.huonghl3@gmail.com"
        );

        if (email.contains("@fpt.edu.vn")) return true;
        return whileList.contains(email.toLowerCase());
    }

    /**
     * Goto home page if login Success
     */
    private void gotoHome() {
        Intent i = new Intent(this, HomeActivity.class);
        startActivity(i);
        finish();
    }

    /**
     * Check login
     * @return is login or not
     */
    private boolean isLogin() {
        MapEntity mapEntity = App.sqlite(this).getMapEntityDao().load("teacher_code");
        return mapEntity != null;
    }

    /**
     * Init Google API Client
     */
    private void googleInit() {
        //Set up login with Google
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient = new GoogleApiClient.Builder( this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API , googleSignInOptions).build();
        Log.d(TAG, "Google Api Client is created!");
    }

    /**
     * Logout google account
     */
    private void logout() {
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(status -> Log.d(TAG, "Log Out Status: " + status.getStatusCode()));
    }

    /**
     * Login event when click button Login
     */
    private void login() {
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(intent, SIGN_IN_CODE);
    }

    /**
     * Login Google Result
     * @param data intent after login
     */
    private void loginResult(Intent data) {
        GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
        assert result != null;
        if (result.isSuccess()) {
            GoogleSignInAccount account = result.getSignInAccount();
            assert account != null;
            Log.d(TAG, "Email Login: " + account.getEmail() + '?');

            //Check email is allowed to login
            if (checkAuth(Objects.requireNonNull(account.getEmail()))) {
                String key = account.getId();
                Log.d(TAG, "Auth Id: " + key);
                requestGetInfo(key);
            } else {
                new Popup(this,
                        getResources().getString(R.string.warning_title),
                        getResources().getString(R.string.not_permission_access),
                        PopupStatus.WARNING,
                        PopupType.OK).show();
                logout();
            }
        }
    }

    /**
     * Send request auth
     * @param key id of google auth
     */
    private void requestGetInfo(String key) {
        GGAuthController authController = API.build(this).create(GGAuthController.class);
        authController.getByGgKey(key).enqueue(new Callback<GGAuthDto>() {
            @Override
            public void onResponse(Call<GGAuthDto> call, Response<GGAuthDto> response) {
                if (response.code() == 200) {
                    GGAuthDto authDto = response.body();
                    assert authDto != null;
                    TeacherDto teacherDto = authDto.getTeacher();
                    App.sqlite(LoginActivity.this).insert(new MapEntity("teacher_code", teacherDto.getCode() + ""));
                    Log.d(TAG, "Save key of " + teacherDto.getCode() + " success!");
                    gotoHome();
                } else if (response.code() == 404) {
                    new Popup(LoginActivity.this,
                            getResources().getString(R.string.warning_title),
                            getResources().getString(R.string.not_permission_access),
                            PopupStatus.WARNING,
                            PopupType.OK).show();
                    logout();
                } else if (response.code() == 500) {
                    new Popup(LoginActivity.this,
                            getResources().getString(R.string.error_title),
                            getResources().getString(R.string.server_error),
                            PopupStatus.DANGER,
                            PopupType.OK).show();
                    logout();
                }
            }

            @Override
            public void onFailure(Call<GGAuthDto> call, Throwable t) {
                new Popup(LoginActivity.this,
                        getResources().getString(R.string.error_title),
                        getResources().getString(R.string.cannot_server_connect),
                        PopupStatus.DANGER,
                        PopupType.OK).show();
                logout();
            }
        });
    }


    interface BookController {
        @GET("/api/book/books")
        Call<ResponseBody> getAll();
    }

}
