package tech.tplus.akkoattendance;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import org.modelmapper.ModelMapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tech.tplus.akkoattendance.controller.TeacherController;
import tech.tplus.akkoattendance.dto.TeacherDto;
import tech.tplus.akkoattendance.entity.MapEntity;
import tech.tplus.akkoattendance.entity.Teacher;
import tech.tplus.akkoattendance.helper.API;
import tech.tplus.akkoattendance.helper.App;
import tech.tplus.akkoattendance.helper.FileUtils;
import tech.tplus.akkoattendance.other.PopupResult;
import tech.tplus.akkoattendance.other.PopupStatus;
import tech.tplus.akkoattendance.other.PopupType;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;


public class InfoFragment extends Fragment {
    private static final String TAG = InfoFragment.class.getSimpleName();
    private static final int AVATAR_PICKER_REQUEST_CODE = 46361;
    private static final int COVER_PICKER_REQUEST_CODE = 46362;

    ImageView avatar, coverImage, avatarCamera, coverImageCamera;
    TextView txtName, txtCode, txtBirthday;
    Button btnEdit, btnLogout;
    EditText edName, edEmail, edPhone, edAddress,edGender, edEntryYear, edExperience;
    Spinner spGender;
    View view;


    private InfoFragListener listener;
    private boolean isEdit = false;
    private boolean isUpdate = false;
    private Teacher teacher;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view == null) view = inflater.inflate(R.layout.fragment_info, container, false);
        if (App.isNetworkConnected(Objects.requireNonNull(getActivity()))) doSynchronized();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        onFind();
        onInit();
    }

    @SuppressLint("StaticFieldLeak")
    private void doSynchronized() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                Log.d(TAG, "=== Start Synchronization ===");
                String teacherCode = App.sqlite(Objects.requireNonNull(getActivity()))
                        .getMapEntityDao()
                        .load("teacher_code")
                        .getValue();

                API.build(getContext())
                        .create(TeacherController.class)
                        .get(teacherCode)
                        .enqueue(callback);
                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof InfoFragListener) {
            listener = (InfoFragListener) context;
        } else {
            Log.d(TAG, "Cannot attach data to HomeActivity");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            assert data != null;
            switch (requestCode) {
                case AVATAR_PICKER_REQUEST_CODE:
                    teacher.setImage(Objects.requireNonNull(data.getData()).toString());
                    break;
                case COVER_PICKER_REQUEST_CODE:
                    teacher.setCoverImage(Objects.requireNonNull(data.getData()).toString());
                    break;
            }
            onInit(teacher);
        }
    }

    /**
     * Get data form SQLite and show in information view
     */
    private void onInit(Teacher teacher) {
        String[] genderList = getResources().getStringArray(R.array.gender_array);
        boolean isUpdate = true;

        //If no Internet and no Synchronized, get Data from SQLite
        if (teacher == null) {
            MapEntity mapEntity = App.sqlite(Objects.requireNonNull(getActivity()))
                    .getMapEntityDao()
                    .load("teacher_id");
            if (mapEntity == null) return;

            String teacherId = mapEntity.getValue();
            teacher = App.sqlite(Objects.requireNonNull(getActivity()))
                    .getTeacherDao()
                    .load(Long.parseLong(teacherId));

            isUpdate = false;
        }

        Teacher finalTeacher = teacher;
        boolean finalIsUpdate = isUpdate;

        //Set info to view, using post to Runtime show view
        avatar.post(() -> {
            if (finalIsUpdate) avatar.setImageResource(R.drawable.avatar_default);
            avatar.setImageURI(Uri.parse(finalTeacher.getImage()));
        });
        coverImage.post(() -> {
            if (finalIsUpdate) coverImage.setImageResource(R.drawable.cover_img);
            coverImage.setImageURI(Uri.parse(finalTeacher.getCoverImage()));
        });
        txtName.post(() -> txtName.setText(finalTeacher.getFullName()));
        txtCode.post(() -> txtCode.setText('@' + finalTeacher.getCode()));
        edName.post(() -> edName.setText(finalTeacher.getFullName()));
        edName.post(() -> edEmail.setText(finalTeacher.getEmail()));
        edPhone.post(() -> edPhone.setText(finalTeacher.getPhone()));
        edAddress.post(() -> edAddress.setText(finalTeacher.getAddress()));
        txtBirthday.post(() -> {
            txtBirthday.setText(finalTeacher.getBirthday());
            txtBirthday.setOnClickListener(v -> {
                if (isEdit) new DatePickerPopup(Objects.requireNonNull(getContext()),
                        txtBirthday.getText().toString(),
                        (result, year, monthOfYear, dayOfMonth) -> {
                            if (result == PopupResult.YES_OK) txtBirthday.setText(dayOfMonth + "/" + (monthOfYear+1) + "/" + year);
                        }).show();
            });
        });

        edGender.post(() -> edGender.setText(genderList[finalTeacher.getGender()]));
        edEntryYear.post(() -> edEntryYear.setText(finalTeacher.getEntryYear()));
        edExperience.post(() -> edExperience.setText(finalTeacher.getExperience().replace(',', '\n')));

        ArrayAdapter<String> adapter = new ArrayAdapter<>(Objects.requireNonNull(getContext()), android.R.layout.simple_spinner_item, genderList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spGender.post(() -> spGender.setAdapter(adapter));


        btnEdit.setOnClickListener(v -> saveEdit());
        btnLogout.setOnClickListener(v -> new Popup(Objects.requireNonNull(getContext()),
                getResources().getString(R.string.warning_title),
                getResources().getString(R.string.log_out_message),
                PopupStatus.WARNING,
                PopupType.OK_CANCEL)
                .setPopupListener(result -> {
                    if (result == PopupResult.YES_OK) listener.infoFrag(true);
                }).show());
        avatarCamera.setOnClickListener(v -> openImageChooser(AVATAR_PICKER_REQUEST_CODE));
        coverImageCamera.setOnClickListener(v -> openImageChooser(COVER_PICKER_REQUEST_CODE));
        this.teacher = teacher;
        Log.d(TAG, "Init info of Teacher Success!");
    }

    /**
     * Call Supper function onInit(Teacher)
     */
    private void onInit() {
        onInit(null);
    }

    /**
     * Find view
     */
    private void onFind() {
        //For two button camera
        avatarCamera = view.findViewById(R.id.ic_avatar_camera);
        coverImageCamera = view.findViewById(R.id.ic_cover_camera);

        //For two image view
        avatar = view.findViewById(R.id.avatar);
        coverImage = view.findViewById(R.id.cover_image);

        //For text view
        txtName = view.findViewById(R.id.txtName);
        txtCode = view.findViewById(R.id.txtCode);

        //For edit text
        edName = view.findViewById(R.id.edName);
        edEmail = view.findViewById(R.id.edEmail);
        edPhone = view.findViewById(R.id.edPhone);
        edAddress = view.findViewById(R.id.edAddress);
        txtBirthday = view.findViewById(R.id.txtBirthday);
        edGender = view.findViewById(R.id.edGender);
        edEntryYear = view.findViewById(R.id.edEntryYear);
        edExperience = view.findViewById(R.id.edExperience);

        //For Spinner
        spGender = view.findViewById(R.id.spGender);

        //For Button
        btnEdit = view.findViewById(R.id.btnEdit);
        btnLogout = view.findViewById(R.id.btnLogout);
    }

    /**
     * Save or Edit
     */
    private void saveEdit() {
        isEdit = !isEdit;
        final EditText[] edListWhite = {edPhone, edAddress, edExperience};
        final EditText[] edListBlack= {edName, edEmail, edEntryYear};
        final ImageView[] icList = {avatarCamera, coverImageCamera};

        if (isEdit) {
            btnEdit.setText(getResources().getString(R.string.btn_save));
            Arrays.stream(icList).forEach(ic -> ic.setVisibility(View.VISIBLE));
            Arrays.stream(edListBlack).forEach(ed -> ed.setBackground(getResources().getDrawable(R.drawable.input_field_3)));
            Arrays.stream(edListWhite).forEach(ed -> {
                ed.setEnabled(true);
                ed.setBackground(getResources().getDrawable(R.drawable.input_field_2));
            });
            edGender.setVisibility(View.GONE);
            spGender.setVisibility(View.VISIBLE);
            txtBirthday.setBackground(getResources().getDrawable(R.drawable.input_field_2));


        } else {
            save();
            btnEdit.setText(getResources().getString(R.string.btn_edit));
            Arrays.stream(icList).forEach(ic -> ic.setVisibility(View.INVISIBLE));
            Arrays.stream(edListBlack).forEach(ed -> ed.setBackground(getResources().getDrawable(R.drawable.input_field_1)));
            Arrays.stream(edListWhite).forEach(ed -> {
                ed.setEnabled(false);
                ed.setBackground(getResources().getDrawable(R.drawable.input_field_1));
            });
            edGender.setVisibility(View.VISIBLE);
            spGender.setVisibility(View.GONE);
            txtBirthday.setBackground(getResources().getDrawable(R.drawable.input_field_1));
        }
    }

    /**
     * Save data into Server
     */
    private void save() {
        boolean isImageNull = false, isCoverNull = false;

        //Put data
        Map<String, RequestBody> data = new ConcurrentHashMap<>();
        data.put("code", createPartFromString(teacher.getCode()));
        data.put("fullName", createPartFromString(edName.getText().toString()));
        data.put("birthday", createPartFromString(txtBirthday.getText().toString()));
        data.put("gender", createPartFromString(spGender.getSelectedItemPosition()+""));
        data.put("address", createPartFromString(edAddress.getText().toString()));
        data.put("phone", createPartFromString(edPhone.getText().toString()));
        data.put("email", createPartFromString(edEmail.getText().toString()));
        data.put("statusId", createPartFromString("1"));
        data.put("entryYear", createPartFromString(edEntryYear.getText().toString()));
        data.put("experience", createPartFromString(edExperience.getText().toString()));

        //Put file
        MultipartBody.Part image = getFile(Uri.parse(teacher.getImage()), "image");
        MultipartBody.Part coverImage = getFile(Uri.parse(teacher.getCoverImage()), "coverImage");

        if (image == null) isImageNull = true;
        if (coverImage == null) isCoverNull = true;

        String code = teacher.getCode();

        TeacherController teacherController = API.build(getContext()).create(TeacherController.class);
        Call<TeacherDto> request;

        if (isImageNull && isCoverNull) request = teacherController.upload(code, data);
        else if (!isImageNull && !isCoverNull) request = teacherController.upload(code, data, image, coverImage);
        else if (!isImageNull) request = teacherController.upload(code, data, image);
        else request = teacherController.upload(code, data, coverImage);

        isUpdate = true;
        request.enqueue(callback);
    }

    @SuppressLint("StaticFieldLeak")
    Callback<TeacherDto> callback = new Callback<TeacherDto>() {
        @Override
        public void onResponse(Call<TeacherDto> call, Response<TeacherDto> response) {
            if (response.code() == 200) {
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... voids) {
                        Teacher teacher = new ModelMapper().map(response.body(), Teacher.class);
                        Uri uriImg = App.downloadData(Objects.requireNonNull(getActivity()), teacher.getImage(), "Teachers");
                        Uri uriCoverImg = App.downloadData(Objects.requireNonNull(getActivity()), teacher.getCoverImage(), "Covers");
                        teacher.setImage(uriImg.toString());
                        teacher.setCoverImage(uriCoverImg.toString());
                        App.sqlite(getActivity()).insertOrReplace(teacher);
                        App.sqlite(getActivity()).insertOrReplace(new MapEntity("teacher_id", teacher.getId() + ""));
                        onInit(teacher);
                        Log.d(TAG, "=== End Synchronization ===");
                        return null;
                    }
                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                if (isUpdate) {
                    new Popup(Objects.requireNonNull(getContext()),
                            getResources().getString(R.string.success_title),
                            getResources().getString(R.string.save_info_success),
                            PopupStatus.SUCCESS,
                            PopupType.OK).show();
                    isUpdate = false;
                }
            } else {
                Log.d(TAG, "Response code: " + response.code());
                Log.d(TAG, "=== End Synchronization ===");
            }
        }

        @Override
        public void onFailure(Call<TeacherDto> call, Throwable t) {
            if (isUpdate) {
                new Popup(Objects.requireNonNull(getContext()),
                        getResources().getString(R.string.warning_title),
                        getResources().getString(R.string.cannot_server_connect),
                        PopupStatus.DANGER,
                        PopupType.OK).show();
                isUpdate = false;
            }
            Log.d(TAG, "Unable to connect to the Server!");
            Log.d(TAG, "=== End Synchronization ===");
        }
    };

    @NonNull
    private RequestBody createPartFromString(String text) {
        return RequestBody.create(MultipartBody.FORM, text);
    }

    /**
     * Hepler method to get file
     */
    MultipartBody.Part getFile(Uri uri, String key) {
        try {
            File originalFile = FileUtils.from(Objects.requireNonNull(getContext()), uri);
            RequestBody filePart = RequestBody.create(MediaType.parse(Objects.requireNonNull(getContext().getContentResolver().getType(uri))), originalFile);
            return MultipartBody.Part.createFormData(key, originalFile.getName(), filePart);
        } catch (IOException | NullPointerException ignored) { }
        return null;
    }

    /**
     * Pick URI file
     */
    private void openImageChooser(int requestCode) {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        String[] mimeTypes = {"image/jpg","image/jpeg","image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        startActivityForResult(intent, requestCode);
    }

    /**
     * Interface to communicate with HomeActivity
     */
    public interface InfoFragListener {
        void infoFrag(boolean isLogout);
    }
}
