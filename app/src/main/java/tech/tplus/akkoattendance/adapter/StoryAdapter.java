package tech.tplus.akkoattendance.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import tech.tplus.akkoattendance.R;
import tech.tplus.akkoattendance.entity.Clazz;
import tech.tplus.akkoattendance.entity.Room;
import tech.tplus.akkoattendance.entity.Subject;
import tech.tplus.akkoattendance.entity.SubjectSlot;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class StoryAdapter extends RecyclerView.Adapter<StoryAdapter.ViewHolder>{

    private final List<SubjectSlot> data;
    private final List<Clazz> clazzList;
    private final List<Subject> subjectList;
    private final List<Room> roomList;

    private final int CLAZZ = 0;
    private final int SUBJECT = 1;
    private final int ROOM = 2;
    private final boolean isFree;

    private final List<Integer> background = Arrays.asList(
            R.drawable.story_bg_1,
            R.drawable.story_bg_2,
            R.drawable.story_bg_3,
            R.drawable.story_bg_4,
            R.drawable.story_bg_5,
            R.drawable.story_bg_6,
            R.drawable.story_bg_7,
            R.drawable.story_bg_8
    );

    private final List<Integer> check = new ArrayList<>();
    
    private StoryListener listener;

    public StoryAdapter() {
        this.data = new ArrayList<>();
        this.data.add(new SubjectSlot());
        this.clazzList = null;
        this.subjectList = null;
        this.roomList = null;
        isFree = true;
    }

    public StoryAdapter(List<SubjectSlot> data, List<Clazz> clazzList, List<Subject> subjectList, List<Room> roomList) {
        data.sort(Comparator.comparing(SubjectSlot::getDate));
        this.data = data;
        this.clazzList = clazzList;
        this.subjectList = subjectList;
        this.roomList = roomList;
        isFree = false;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_story, parent, false);
        return new ViewHolder(view);
    }

    public void setItemClickListener(StoryListener listener) {
        this.listener = listener;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (!isFree) {
            //For status and color
            int[] statusRes = {R.string.not_yet_story, R.string.open_story, R.string.close_story};
            int[] drawableRes = {R.drawable.status_story_bg_2, R.drawable.status_story_bg_1, R.drawable.status_story_bg_3};
            int[] colorRes = {R.color.color5, R.color.color15, R.color.color14};

            SubjectSlot slot = data.get(position);
            int i = (int) (slot.getStatusId()-5);
            int classIndex = findIndexById(slot.getClazzId(), CLAZZ);
            int subjectIndex = findIndexById(slot.getSubjectId(), SUBJECT);
            int roomIndex = findIndexById(slot.getRoomId(), ROOM);
            Clazz clazz = clazzList.get(classIndex);
            Subject subject = subjectList.get(subjectIndex);
            Room room = roomList.get(roomIndex);

            holder.txtClass.setText(clazz.getCode());
            holder.txtSubject.setText(subject.getCode());
            holder.txtSlotRoom.setText(holder.view.getResources().getString(R.string.slot) + " " + getSlot(slot.getDate()) + " • " + room.getCode());
            holder.txtStatus.setText(statusRes[i]);
            holder.txtStatus.setTextColor(holder.view.getResources().getColor(colorRes[i]));
            holder.txtIcon.setBackgroundResource(drawableRes[i]);
            holder.view.setOnClickListener(v -> listener.onItemClick(data.get(position), position));

            int bgIndex;
            do {
                bgIndex = new Random().nextInt(background.size());
            } while (check.contains(bgIndex));
            check.add(bgIndex);
            holder.bg.setBackgroundResource(background.get(bgIndex));
        }
    }

    private int getSlot(Date date) {
        final int[] hours =   {7,  8,  10, 12, 14, 16, 18, 20};
        final int[] minutes = {0, 45,  30, 45, 30, 15, 30, 15};
        for(int i=0; i<hours.length; ++i)
            if (date.getHours() == hours[i] && date.getMinutes() == minutes[i]) return i+1;
        return -1;
    }

    private int findIndexById(Long id, int type) {
        AtomicInteger index = new AtomicInteger(-1);
        switch (type) {
            case CLAZZ:
                clazzList.forEach(clazz -> {
                    if (clazz.getId().equals(id)) index.set(clazzList.indexOf(clazz));
                });
                break;
            case SUBJECT:
                subjectList.forEach(subject -> {
                    if (subject.getId().equals(id)) index.set(subjectList.indexOf(subject));
                });
                break;
            case ROOM:
                roomList.forEach(room -> {
                    if (room.getId().equals(id)) index.set(roomList.indexOf(room));
                });
                break;
        }
        return index.get();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtClass, txtSubject, txtSlotRoom, txtStatus, txtIcon;
        ConstraintLayout bg;
        View view;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            //TODO: Find item
            view = itemView;
            txtClass = itemView.findViewById(R.id.txtClass);
            txtSubject = itemView.findViewById(R.id.txtSubject);
            txtSlotRoom = itemView.findViewById(R.id.txtSlotRoom);
            txtStatus = itemView.findViewById(R.id.txtStatus);
            txtIcon = itemView.findViewById(R.id.txtIconStatus);
            bg = itemView.findViewById(R.id.bg);

        }
    }

    
    public interface StoryListener {
        void onItemClick(SubjectSlot slot, int position);
    }
}
