package tech.tplus.akkoattendance.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import tech.tplus.akkoattendance.R;
import tech.tplus.akkoattendance.entity.Clazz;
import tech.tplus.akkoattendance.entity.Clazz;

import java.util.List;

public class ClazzAdapter extends BaseAdapter {

    private final List<Clazz> data;

    public ClazzAdapter(List<Clazz> data) {
        this.data = data;
    }
    

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Clazz getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu, parent, false);

        if (data.size() != 0) {
            TextView txtTitle = convertView.findViewById(R.id.txtMenuTitle);
            TextView txtDescription = convertView.findViewById(R.id.txtMenuDescription);
            ImageView icGeneral = convertView.findViewById(R.id.icGeneral);

            Clazz clazz = data.get(position);
            txtTitle.setText(clazz.getCode());
            txtDescription.setText(clazz.getDescription());
            icGeneral.setVisibility(View.VISIBLE);
        }


        return convertView;
    }
}
