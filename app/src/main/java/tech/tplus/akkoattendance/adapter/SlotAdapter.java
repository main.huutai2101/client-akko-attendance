package tech.tplus.akkoattendance.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import tech.tplus.akkoattendance.R;
import tech.tplus.akkoattendance.entity.Room;
import tech.tplus.akkoattendance.entity.Status;
import tech.tplus.akkoattendance.entity.SubjectSlot;
import tech.tplus.akkoattendance.helper.App;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class SlotAdapter extends BaseAdapter {

    private final List<SubjectSlot> data;
    private final List<Room> roomList;

    public SlotAdapter(List<SubjectSlot> data, List<Room> roomList) {
        this.data = data;
        this.roomList = roomList;
    }
    

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public SubjectSlot getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_slot, parent, false);

        TextView txtDate = convertView.findViewById(R.id.txt_date);
        TextView txtSlotNumber = convertView.findViewById(R.id.txt_slot_number);
        TextView txtSlot = convertView.findViewById(R.id.txt_slot_time);
        TextView txtStatus = convertView.findViewById(R.id.txt_status);
        TextView txtRoom = convertView.findViewById(R.id.txt_room);
        ImageView imgStatus = convertView.findViewById(R.id.img_status);
        ConstraintLayout layout = convertView.findViewById(R.id.layout_bg);


        SubjectSlot slot = data.get(position);

        //For time format
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(slot.getDate());
        calendar.add(Calendar.MINUTE, 90);
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE - dd/MM/yyyy");
        SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm a");

        //For status and color
        int[] statusRes = {R.string.not_yet, R.string.open, R.string.close};
        int[] colorRes = {R.color.color5, R.color.color3, R.color.color14};
        int[] shapeRes = {R.drawable.shape_5, R.drawable.shape_2, R.drawable.shape_4};

        Long statusId = slot.getStatusId();

        txtSlotNumber.setText(slot.getSlotNumber()+"");
        txtRoom.setText(getStr(convertView, R.string.room) + " " + getRoomCode(slot.getRoomId()));
        txtDate.setText(dateFormat.format(slot.getDate()));
        txtSlot.setText(getStr(convertView, R.string.slot) + " " +getSlot(slot.getDate()) + ": " + timeFormat.format(slot.getDate()) + " - " + timeFormat.format(calendar.getTime()));
        txtStatus.setText(getStr(convertView, statusRes[(int) (statusId-5)]));
        txtStatus.setTextColor(convertView.getResources().getColor(colorRes[(int) (statusId-5)]));
        imgStatus.setImageDrawable(convertView.getResources().getDrawable(shapeRes[(int) (statusId-5)]));

        Calendar today = Calendar.getInstance();
        if (slot.getDate().getDate() == today.getTime().getDate() &&
            slot.getDate().getMonth() == today.getTime().getMonth() &&
            slot.getDate().getYear() == today.getTime().getYear()) {
            layout.setBackgroundResource(R.color.color16);
        }

        return convertView;
    }

    private String getStr(View v, int id) {
        return v.getResources().getString(id);
    }

    private int getSlot(Date date) {
        final int[] hours =   {7,  8,  10, 12, 14, 16, 18, 20};
        final int[] minutes = {0, 45,  30, 45, 30, 15, 30, 15};
        for(int i=0; i<hours.length; ++i)
            if (date.getHours() == hours[i] && date.getMinutes() == minutes[i]) return i+1;
            return -1;
    }

    private String getRoomCode(Long id) {
        for (Room room : roomList) {
            if (room.getId().equals(id)) return room.getCode();
        }

        return "NaN";
    }
}
