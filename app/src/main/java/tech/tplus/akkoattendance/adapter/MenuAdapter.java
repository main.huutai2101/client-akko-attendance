package tech.tplus.akkoattendance.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import tech.tplus.akkoattendance.R;
import tech.tplus.akkoattendance.entity.Menu;

import java.util.Arrays;
import java.util.List;

public class MenuAdapter extends BaseAdapter {

    private final List<Menu> data;
    private final List<Integer> icon;
    private final Context context;

    public MenuAdapter(Context context) {
        this.context = context;
        this.data = Arrays.asList(
                new Menu(getResourceString(R.string.item_timetable),getResourceString(R.string.item_timetable_description)),
                new Menu(getResourceString(R.string.item_attendance),getResourceString(R.string.item_attendance_description)),
                new Menu(getResourceString(R.string.item_report),getResourceString(R.string.item_report_description)),
                new Menu(getResourceString(R.string.item_class),getResourceString(R.string.item_class_description)),
                new Menu(getResourceString(R.string.item_subject),getResourceString(R.string.item_subject_description))
        );

        this.icon = Arrays.asList(
                R.drawable.ic_timetable,
                R.drawable.ic_attendance,
                R.drawable.ic_report,
                R.drawable.ic_class,
                R.drawable.ic_subject
        );
    }

    private String getResourceString(int id) {
        return context.getResources().getString(id);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Menu getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu, parent, false);
        TextView txtTitle = convertView.findViewById(R.id.txtMenuTitle);
        TextView txtDescription = convertView.findViewById(R.id.txtMenuDescription);
        ImageView icMenu = convertView.findViewById(R.id.icMenu);

        Menu m = data.get(position);
        txtTitle.setText(m.getTitle());
        txtDescription.setText(m.getDescription());
        icMenu.setImageResource(icon.get(position));

        return convertView;
    }
}
