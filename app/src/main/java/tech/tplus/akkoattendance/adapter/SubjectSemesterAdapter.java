package tech.tplus.akkoattendance.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import tech.tplus.akkoattendance.R;
import tech.tplus.akkoattendance.entity.Clazz;
import tech.tplus.akkoattendance.entity.Subject;
import tech.tplus.akkoattendance.entity.SubjectSemester;

import java.text.SimpleDateFormat;
import java.util.List;

public class SubjectSemesterAdapter extends BaseAdapter {
    private static final int SUBJECT = 0;
    private static final int CLAZZ = 1;

    private final List<SubjectSemester> ssList;
    private final List<Subject> subjectsList;
    private final List<Clazz> clazzList;

    public SubjectSemesterAdapter(List<SubjectSemester> ssList, List<Subject> subjectsList, List<Clazz> clazzList) {
        this.ssList = ssList;
        this.clazzList = clazzList;
        this.subjectsList = subjectsList;
    }

    private int findIndexById(Long id, int type) {
        int i;
        if (type == SUBJECT) {
            for (i=0; i<subjectsList.size(); ++i)
                if (subjectsList.get(i).getId().equals(id)) return i;
        } else if (type == CLAZZ) {
            for (i=0; i<clazzList.size(); ++i)
                if (clazzList.get(i).getId().equals(id)) return i;
        }
        return  -1;
    }
    

    @Override
    public int getCount() {
        return ssList.size();
    }

    @Override
    public SubjectSemester getItem(int position) {
        return ssList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu, parent, false);

        if (ssList.size() !=0 && subjectsList.size() !=0 && clazzList.size() != 0) {
            TextView txtTitle = convertView.findViewById(R.id.txtMenuTitle);
            TextView txtDescription = convertView.findViewById(R.id.txtMenuDescription);
            ImageView icGeneral = convertView.findViewById(R.id.icGeneral);

            SubjectSemester ss = ssList.get(position);

            Subject subject = subjectsList.get(findIndexById(ss.getSubjectId(), SUBJECT));
            Clazz clazz = clazzList.get(findIndexById(ss.getClazzId(), CLAZZ));
            txtTitle.setText(clazz.getCode() + " - " + subject.getCode());

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            String desc = parent.getContext().getResources().getString(R.string.start_date) + ' '
                    + sdf.format(ss.getStartDate()) + " • "
                    + ss.getNumberOfSlots() + ' '
                    + parent.getResources().getString(R.string.slots);

            txtDescription.setText(desc);
            icGeneral.setVisibility(View.VISIBLE);
        }

        return convertView;
    }
}
