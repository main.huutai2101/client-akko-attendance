package tech.tplus.akkoattendance.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import tech.tplus.akkoattendance.R;
import tech.tplus.akkoattendance.entity.Clazz;
import tech.tplus.akkoattendance.entity.Room;
import tech.tplus.akkoattendance.entity.Subject;
import tech.tplus.akkoattendance.entity.SubjectSlot;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class EventAdapter extends BaseAdapter {

    private final List<SubjectSlot> data;
    private final List<Room> roomList;
    private final List<Subject> subjectList;
    private final List<Clazz> clazzList;

    public EventAdapter(List<SubjectSlot> data, List<Room> roomList, List<Subject> subjectList, List<Clazz> clazzList) {
        this.data = data;
        this.data.sort(Comparator.comparing(SubjectSlot::getDate));
        this.roomList = roomList;
        this.clazzList = clazzList;
        this.subjectList = subjectList;
    }
    

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public SubjectSlot getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_event, parent, false);

        ImageView imgShape = convertView.findViewById(R.id.img_shape);
        TextView txtSlot = convertView.findViewById(R.id.txt_slot);
        TextView txtStatus = convertView.findViewById(R.id.txt_status);
        TextView txtSubject = convertView.findViewById(R.id.txt_subject);
        TextView txtClassRoom = convertView.findViewById(R.id.txt_class_room);

        //Set shape color
        int[] shapesDrawable = {R.drawable.shape_1, R.drawable.shape_2, R.drawable.shape_3};
        imgShape.setImageResource(shapesDrawable[position%3]);

        //Set status
        int[] statusString = {R.string.not_yet, R.string.open, R.string.close};
        int[] statusColor = {R.color.color5, R.color.color15, R.color.color14};
        int i = (int) (data.get(position).getStatusId()-5);
        txtStatus.setText(convertView.getResources().getString(statusString[i]));
        txtStatus.setTextColor(convertView.getResources().getColor(statusColor[i]));

        //Set slot number
        int slotNumber = getSlot(data.get(position).getDate());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(data.get(position).getDate());
        calendar.add(Calendar.MINUTE, 90);
        SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm a");
        txtSlot.setText(convertView.getResources().getString(R.string.slot) + " " + slotNumber + ": " + timeFormat.format(data.get(position).getDate()) + " - " + timeFormat.format(calendar.getTime()));

        //Set subject
        Subject subject = getSubject(data.get(position).getSubjectId());
        if (subject != null) txtClassRoom.setText(subject.getCode() + " - " + subject.getName());

        //Get class, room
        Clazz clazz = getClazz(data.get(position).getClazzId());
        Room room = getRoom(data.get(position).getRoomId());
        if (clazz != null && room != null) txtSubject.setText(clazz.getCode() + " - " + room.getCode());
        return convertView;
    }

    private Subject getSubject(Long id) {
        for(Subject s: subjectList)
            if (s.getId().equals(id)) return s;
        return null;
    }

    private Clazz getClazz(Long id) {
        for(Clazz c: clazzList)
            if (c.getId().equals(id)) return c;
        return null;
    }

    private Room getRoom(Long id) {
        for(Room r: roomList)
            if (r.getId().equals(id)) return r;
        return null;
    }

    private int getSlot(Date date) {
        final int[] hours =   {7,  8,  10, 12, 14, 16, 18, 20};
        final int[] minutes = {0, 45,  30, 45, 30, 15, 30, 15};
        for(int i=0; i<hours.length; ++i)
            if (date.getHours() == hours[i] && date.getMinutes() == minutes[i]) return i+1;
        return -1;
    }
}
