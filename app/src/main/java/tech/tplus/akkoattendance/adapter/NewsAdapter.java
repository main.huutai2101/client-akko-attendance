package tech.tplus.akkoattendance.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import tech.tplus.akkoattendance.R;
import tech.tplus.akkoattendance.entity.News;

import java.text.SimpleDateFormat;
import java.util.List;

public class NewsAdapter extends BaseAdapter {

    private final LayoutInflater li;
    private final List<News> data;
    public NewsAdapter(Context context, List<News> data) {
        this.data = data;
        this.li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public News getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public void addItem(News news) {
        data.add(news);
    }

    public void addItem(List<News> news) {
        data.addAll(news);
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = li.inflate(R.layout.item_news_notify, parent, false);

        TextView txtTitle = convertView.findViewById(R.id.txt_news_title);
        TextView txtContent = convertView.findViewById(R.id.txt_news_content);
        TextView txtDatetime = convertView.findViewById(R.id.txt_datetime);


        News news = data.get(position);
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:ss");

        //Set data into view
        if (!news.getIsRead()) txtTitle.setTextColor(convertView.getResources().getColor(R.color.color6));

        String content;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) content = Html.fromHtml(news.getContent(), Html.FROM_HTML_MODE_COMPACT) + "";
        else content = Html.fromHtml(news.getContent())+ "";


        txtContent.setText(content.subSequence(0, Math.min(50, content.length())) + "...");
        txtTitle.setText(news.getTitle());
        txtDatetime.setText(format.format(news.getCreatedAt()) + " • @" + news.getCreatedBy());


        return convertView;
    }
}
