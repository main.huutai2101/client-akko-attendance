package tech.tplus.akkoattendance.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import tech.tplus.akkoattendance.R;
import tech.tplus.akkoattendance.entity.Subject;

import java.util.List;

public class SubjectAdapter extends BaseAdapter {

    private final List<Subject> data;

    public SubjectAdapter(List<Subject> data) {
        this.data = data;
    }
    

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Subject getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu, parent, false);

        if (data.size() != 0) {
            TextView txtTitle = convertView.findViewById(R.id.txtMenuTitle);
            TextView txtDescription = convertView.findViewById(R.id.txtMenuDescription);
            ImageView icGeneral = convertView.findViewById(R.id.icGeneral);

            Subject subject = data.get(position);
            txtTitle.setText(subject.getCode());
            txtDescription.setText(subject.getName());
            icGeneral.setVisibility(View.VISIBLE);
        }

        return convertView;
    }
}
