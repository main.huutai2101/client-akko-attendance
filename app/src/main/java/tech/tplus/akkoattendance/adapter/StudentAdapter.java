package tech.tplus.akkoattendance.adapter;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import tech.tplus.akkoattendance.R;
import tech.tplus.akkoattendance.entity.Student;
import tech.tplus.akkoattendance.entity.SubjectSession;

import java.io.File;
import java.net.URI;
import java.util.List;

public class StudentAdapter extends BaseAdapter {
    private static final String TAG = StudentAdapter.class.getSimpleName();

    private final List<Student> students;
    private final List<SubjectSession> sessions;
    private boolean isTake;

    public StudentAdapter(List<Student> students, List<SubjectSession> sessions, boolean isTake) {
        this.students = students;
        this.sessions = sessions;
        this.isTake = isTake;
        Log.d(TAG, "Students size: " + students.size());
        Log.d(TAG, "Sessions size: " + sessions.size());
    }

    public StudentAdapter(List<Student> students) {
        this.students = students;
        this.sessions = null;
    }

    @Override
    public int getCount() {
        return students.size();
    }

    @Override
    public Student getItem(int position) {
        return students.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_student, parent, false);

        ImageView imgAvatar = convertView.findViewById(R.id.img_avatar);
        ImageView imgStatus = convertView.findViewById(R.id.img_status);
        TextView txtName = convertView.findViewById(R.id.txt_name);
        TextView txtCode = convertView.findViewById(R.id.txt_code);


        Student s = students.get(position);

        //Check image existed or not
        File f = new File(URI.create(s.getImage()));
        if (f.exists()) imgAvatar.setImageURI(Uri.parse(s.getImage()));

        txtName.setText(s.getFullName());
        txtCode.setText(s.getCode());

        if (sessions != null) {
            SubjectSession ss = sessions.get(position);
            int id = R.drawable.ic_not_yet;
            if (ss.getStatusId() == 3) id = R.drawable.ic_attended;
            else if (ss.getStatusId() == 4) id = R.drawable.ic_absent;
            imgStatus.setImageResource(id);
            imgStatus.setVisibility(View.VISIBLE);
            convertView.setOnClickListener(v -> {
                if (isTake) {
                    if (ss.getStatusId().equals(3L)) {
                        ss.setStatusId(4L);
                        imgStatus.setImageResource(R.drawable.ic_absent);
                    } else {
                        ss.setStatusId(3L);
                        imgStatus.setImageResource(R.drawable.ic_attended);
                    }
                }
            });

        }



        return convertView;
    }

    private StudentAdapterListener listener;

    public void setItemStatusChange(StudentAdapterListener listener) {
        this.listener = listener;
    }

    public interface StudentAdapterListener {
        void onStatusChange(int index, Long statusId);
    }
}
