package tech.tplus.akkoattendance;

import android.os.Bundle;
import android.util.Log;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class InfoContainerFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        assert getFragmentManager() != null;
        getFragmentManager().beginTransaction()
                .replace(R.id.info_container, new InfoFragment())
                .commit();
        Log.d("InfoFragment", "Container Init");
        return inflater.inflate(R.layout.fragment_info_container, container, false);
    }
}
