package tech.tplus.akkoattendance.dto;

import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class SubjectSemesterDto {
    private Long id;
    private Integer numberOfSlots;
    private String advisor;
    private Date startDate;

    private StatusDto status;
    private SemesterDto semester;
    private SubjectDto subject;
    private TeacherDto teacher;
    private ClazzDto clazz;
    private List<StudentDto> students;
}
