package tech.tplus.akkoattendance.dto;

import lombok.Data;

@Data
public class SubjectDto {
    private Long id;
    private String code;
    private String name;
    private String description;

    private StatusDto status;
}
