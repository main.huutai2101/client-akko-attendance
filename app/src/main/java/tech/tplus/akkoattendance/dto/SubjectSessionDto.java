package tech.tplus.akkoattendance.dto;

import lombok.Data;

@Data
public class SubjectSessionDto {
    private Long id;
    private StatusDto status;
    private StudentDto student;
    private SubjectSlotDto slot;

}
