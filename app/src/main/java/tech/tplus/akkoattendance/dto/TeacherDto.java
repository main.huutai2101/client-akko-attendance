package tech.tplus.akkoattendance.dto;

import lombok.Data;

@Data
public class TeacherDto {
    private Long id;
    private String code;
    private String fullName;
    private String birthday;
    private int gender;
    private String address;
    private String phone;
    private String email;
    private String image;
    private String entryYear;
    private String experience;
    private String coverImage;

    private StatusDto status;
}
