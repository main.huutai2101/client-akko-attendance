package tech.tplus.akkoattendance.dto;

import lombok.Data;

@Data
public class MajorDto {
    private Long id;
    private String code;
    private String description;

    private StatusDto status;
}
