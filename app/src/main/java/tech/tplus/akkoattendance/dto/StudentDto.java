package tech.tplus.akkoattendance.dto;

import lombok.Data;

@Data
public class StudentDto {
    private Long id;
    private String code;
    private String fullName;
    private String birthday;
    private String gender;
    private String address;
    private String phone;
    private String email;
    private String image;

    private MajorDto major;
    private ClazzDto clazz;
    private StatusDto status;
}
