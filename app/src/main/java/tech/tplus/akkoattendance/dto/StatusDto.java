package tech.tplus.akkoattendance.dto;

import lombok.Data;

@Data
public class StatusDto {
    private Long id;
    private String code;
    private String description;
}
