package tech.tplus.akkoattendance.dto;

import lombok.Data;

@Data
public class RoleDto {
    private Long id;
    private String roleName;
    private String description;

    private StatusDto status;
}
