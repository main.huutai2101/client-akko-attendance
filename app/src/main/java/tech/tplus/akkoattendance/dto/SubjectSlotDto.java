package tech.tplus.akkoattendance.dto;

import java.util.Date;

import lombok.Data;

@Data
public class SubjectSlotDto {
    private Long id;
    private Integer slotNumber;
    private Date date;

    private StatusDto status;
    private SubjectSemesterDto subjectSemester;
    private RoomDto room;
}
