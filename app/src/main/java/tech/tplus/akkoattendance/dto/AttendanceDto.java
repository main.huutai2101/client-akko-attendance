package tech.tplus.akkoattendance.dto;

import tech.tplus.akkoattendance.entity.SubjectSession;

import java.util.List;

public class AttendanceDto {
    Long slotId;
    List<SubjectSession> sessions;

    public AttendanceDto(Long slotId, List<SubjectSession> sessions) {
        this.slotId = slotId;
        this.sessions = sessions;
    }

    public AttendanceDto() {
    }

    public Long getSlotId() {
        return slotId;
    }

    public void setSlotId(Long slotId) {
        this.slotId = slotId;
    }

    public List<SubjectSession> getSessions() {
        return sessions;
    }

    public void setSessions(List<SubjectSession> sessions) {
        this.sessions = sessions;
    }
}
