package tech.tplus.akkoattendance.dto;

import lombok.Data;

@Data
public class RoomDto {
    private Long id;
    private String code;
    private String floor;
    private Integer capacity;

    private StatusDto status;
}
