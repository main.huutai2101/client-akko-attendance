package tech.tplus.akkoattendance.dto;

import lombok.Data;

@Data
public class GGAuthDto {
    private Long id;
    private String ggauthId;

    private RoleDto role;
    private TeacherDto teacher;
}
