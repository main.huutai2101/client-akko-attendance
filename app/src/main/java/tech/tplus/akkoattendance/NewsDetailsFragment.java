package tech.tplus.akkoattendance;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import tech.tplus.akkoattendance.entity.News;

import java.text.SimpleDateFormat;

public class NewsDetailsFragment extends Fragment {
    private static final String TAG = NewsDetailsFragListener.class.getSimpleName();

    private final News news;
    private NewsDetailsFragListener listener;

    public NewsDetailsFragment(News news) {
        this.news = news;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_news_details, container, false);

        TextView txtTitle = v.findViewById(R.id.txt_news_title);
        TextView txtContent = v.findViewById(R.id.txt_news_content);
        TextView txtDatetime = v.findViewById(R.id.txt_datetime);
        ImageView imgBack = v.findViewById(R.id.img_back);

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:ss");

        //Set data into view
        txtTitle.setText('\n' + news.getTitle());
        String content = news.getContent();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) txtContent.setText(Html.fromHtml(content, Html.FROM_HTML_MODE_COMPACT));
        else txtContent.setText(Html.fromHtml(content));
        txtContent.setMovementMethod(LinkMovementMethod.getInstance());
        txtDatetime.setText(format.format(news.getCreatedAt()) + " • @" + news.getCreatedBy());
        imgBack.setOnClickListener(e -> listener.newsDetailsFrag(true));

        return v;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof NewsDetailsFragment.NewsDetailsFragListener) {
            listener = (NewsDetailsFragment.NewsDetailsFragListener) context;
        } else {
            Log.d(TAG, "Cannot attach data to HomeActivity");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public interface NewsDetailsFragListener {
        void newsDetailsFrag(boolean isBackPress);
    }
}
