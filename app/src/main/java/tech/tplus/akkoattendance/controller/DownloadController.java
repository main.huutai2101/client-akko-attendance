package tech.tplus.akkoattendance.controller;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface DownloadController {
    @GET("/download/{uri}")
    Call<ResponseBody> get(@Path("uri") String uri);
}
