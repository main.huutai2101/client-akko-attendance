package tech.tplus.akkoattendance.controller;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;

public interface HomeController {
    @GET("/")
    Call<ResponseBody> serverCheck();
}
