package tech.tplus.akkoattendance.controller;

import retrofit2.Call;
import retrofit2.http.*;
import tech.tplus.akkoattendance.dto.AttendanceDto;
import tech.tplus.akkoattendance.entity.SubjectSession;

import java.util.List;

public interface SessionController {
    @GET("/subject-session/teacher/{code}")
    Call<List<SubjectSession>> getAllSessionByTeacherCode(@Path("code") String code);

    @PUT("/subject-session/put")
    Call<AttendanceDto> post(@Body AttendanceDto attendanceDto);
}

