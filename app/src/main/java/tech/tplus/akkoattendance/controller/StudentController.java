package tech.tplus.akkoattendance.controller;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import tech.tplus.akkoattendance.dto.StudentDto;

import java.util.List;

public interface StudentController {
    @GET("/student/class/{id}")
    Call<List<StudentDto>> getStudentsByClass(@Path("id") Long classId);
}
