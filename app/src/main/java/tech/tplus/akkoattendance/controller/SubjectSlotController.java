package tech.tplus.akkoattendance.controller;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import tech.tplus.akkoattendance.dto.SubjectSlotDto;

import java.util.List;

public interface SubjectSlotController {
    @GET("/teacher/{code}/all-slots")
    Call<List<SubjectSlotDto>> getAllSlots(@Path("code") String code);
}
