package tech.tplus.akkoattendance.controller;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import tech.tplus.akkoattendance.dto.SubjectSemesterDto;
import tech.tplus.akkoattendance.dto.SubjectSlotDto;
import tech.tplus.akkoattendance.dto.TeacherDto;

public interface TeacherController {
    @Multipart
    @PUT("/teacher/{code}")
    Call<TeacherDto> upload(
            @Path("code") String code,
            @PartMap Map<String, RequestBody> data,
            @Part MultipartBody.Part image,
            @Part MultipartBody.Part coverImage
    );

    @Multipart
    @PUT("/teacher/{code}")
    Call<TeacherDto> upload(
            @Path("code") String code,
            @PartMap Map<String, RequestBody> data,
            @Part MultipartBody.Part img
    );

    @Multipart
    @PUT("/teacher/{code}")
    Call<TeacherDto> upload(
            @Path("code") String code,
            @PartMap Map<String, RequestBody> data
    );

    @GET("/teacher/{code}")
    Call<TeacherDto> get(@Path("code") String code);

    @GET("/teacher/{code}/subject-slots")
    Call<List<SubjectSlotDto>> getSlotsToday(@Path("code") String code);

    @GET("/teacher/{code}/subject-semesters")
    Call<List<SubjectSemesterDto>> getSubjectSemester(@Path("code") String code);

}
