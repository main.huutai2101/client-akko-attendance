package tech.tplus.akkoattendance.controller;

import retrofit2.Call;
import retrofit2.http.GET;
import tech.tplus.akkoattendance.entity.Status;

import java.util.List;

public interface StatusController {
    @GET("/status")
    Call<List<Status>> getAll();
}
