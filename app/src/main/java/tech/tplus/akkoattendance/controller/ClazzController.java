package tech.tplus.akkoattendance.controller;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import tech.tplus.akkoattendance.entity.Clazz;
import tech.tplus.akkoattendance.entity.Subject;

import java.util.List;

public interface ClazzController {
    @GET("/class")
    Call<List<Clazz>> getAll();

    @GET("class/teacher/{code}")
    Call<List<Clazz>> getAllByTeacherCode(@Path("code") String code);
}
