package tech.tplus.akkoattendance.controller;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import tech.tplus.akkoattendance.dto.GGAuthDto;

public interface GGAuthController {
    @GET("/gg-auth/{key}")
    Call<GGAuthDto> getByGgKey(@Path("key") String key);
}
