package tech.tplus.akkoattendance.controller;

import retrofit2.Call;
import retrofit2.http.GET;
import tech.tplus.akkoattendance.entity.Room;

import java.util.List;

public interface RoomController {
    @GET("/room")
    Call<List<Room>> getAll();
}
