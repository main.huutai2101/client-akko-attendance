package tech.tplus.akkoattendance.controller;

import retrofit2.Call;
import retrofit2.http.GET;
import tech.tplus.akkoattendance.entity.Major;
import tech.tplus.akkoattendance.entity.Status;

import java.util.List;

public interface MajorController {
    @GET("/major")
    Call<List<Major>> getAll();
}
