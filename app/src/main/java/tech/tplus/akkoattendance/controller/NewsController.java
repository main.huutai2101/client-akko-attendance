package tech.tplus.akkoattendance.controller;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import tech.tplus.akkoattendance.entity.News;

import java.util.List;

public interface NewsController {
    @GET("/news/latest/{n}")
    Call<List<News>> latestNewsWithLimit(@Path("n") int n);

    @GET("/news/update/{id}")
    Call<List<News>> updateNews(@Path("id")  Long id);

    @GET("/news/scroll")
    Call<List<News>> scrollUpdate(@Query("id") Long id, @Query("size") Integer size);
}
