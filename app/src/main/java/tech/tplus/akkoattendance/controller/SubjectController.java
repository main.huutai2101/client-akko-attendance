package tech.tplus.akkoattendance.controller;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import tech.tplus.akkoattendance.entity.Subject;

import java.util.List;

public interface SubjectController {
    @GET("subject")
    Call<List<Subject>> getAll();

    @GET("subject/teacher/{code}")
    Call<List<Subject>> getAllByTeacherCode(@Path("code") String code);
}
