package tech.tplus.akkoattendance;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.ashokvarma.bottomnavigation.BottomNavigationBar;
import com.ashokvarma.bottomnavigation.BottomNavigationItem;
import com.ashokvarma.bottomnavigation.ShapeBadgeItem;
import com.ashokvarma.bottomnavigation.TextBadgeItem;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import tech.tplus.akkoattendance.adapter.SwipeDisabledViewPager;
import tech.tplus.akkoattendance.adapter.ViewPagerAdapter;
import tech.tplus.akkoattendance.entity.MapEntity;
import tech.tplus.akkoattendance.entity.StudentDao;
import tech.tplus.akkoattendance.helper.App;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Objects;


public class HomeActivity extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener,
        InfoFragment.InfoFragListener,
        TimetableFragment.TimetableFragListener,
        NewsFragment.NewsFragListener,
        SubjectFragment.SubjectFragListener,
        ClassFragment.ClassFragListener,
        AttendanceFragment.AttendanceFragListener,
        HomeFragment.HomeFragListener,
        StudentFragment.StudentFragListener,
        NewsDetailsFragment.NewsDetailsFragListener {

    private static final String TAG = HomeActivity.class.getSimpleName();
    private static final int WAIT = 3000;

    ViewPagerAdapter adapter;
    SwipeDisabledViewPager viewPager;
    TextBadgeItem newsBadgeItem, notifyBadgeItem;
    ShapeBadgeItem homeBadgeItem;
    BottomNavigationBar bottomNavigationBar;
    ConstraintLayout layoutLoading;
    ProgressBar progressBar;
    TextView txtDownloadInfo;



    private GoogleApiClient googleApiClient;
    private boolean doubleBackToExitPressedOnce = false;
    private boolean isFirstLogin = false;
    //Subject, Status, Class, Major, News, Room, SubjectSemester, Slot, Student, Session
    private boolean isSubjectDownloaded = false;
    private boolean isStatusDownloaded = false;
    private boolean isClassDownloaded = false;
    private boolean isMajorDownloaded = false;
    private boolean isNewsDownloaded = false;
    private boolean isRoomDownloaded = false;
    private boolean isSubjectSemesterDownloaded = false;
    private boolean isSlotDownloaded = false;
    private boolean isStudentDownloaded = false;
    private boolean isSessionDownloaded = false;


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }



    @SuppressLint("StaticFieldLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        loading();

        createBottomNavigationBar();

        googleInit();

        viewPager = findViewById(R.id.view_pager);
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new HomeContainerFragment(), "HomeContainerFragment");
        adapter.addFrag(new NewsContainerFragment(), "NewsContainerFragment");
        adapter.addFrag(new NotifyContainerFragment(), "NotifyContainerFragment");
        adapter.addFrag(new InfoContainerFragment(), "InfoContainerFragment");
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(4);
    }

    private void loading() {
        MapEntity mapEntity = App.sqlite(this).getMapEntityDao().load("isFirst");
        if (mapEntity == null) isFirstLogin = true;

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        layoutLoading = findViewById(R.id.layout_loading);
        progressBar = findViewById(R.id.progressBar);
        txtDownloadInfo = findViewById(R.id.txtDownloadInfo);
        ImageView imgLoading = findViewById(R.id.imgLoading);

        AnimationDrawable animationDrawable = (AnimationDrawable) imgLoading.getDrawable();
        animationDrawable.start();
        layoutLoading.setVisibility(View.VISIBLE);

    }

    @Override
    public void infoFrag(boolean isLogout) {
        if (isLogout) logout();
    }

    private boolean isInitAfterSync = false;
    @Override
    public void newsFrag(int unreadNews) {
        bottomNavigationBar.post(() -> {
            if (unreadNews == 0) newsBadgeItem.hide(true);
            else {
                newsBadgeItem.setText(unreadNews + "");
                newsBadgeItem.show(false);
                if (!isInitAfterSync) {
                    bottomNavigationBar.initialise();
                    isInitAfterSync = true;
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        int d = Objects.requireNonNull(adapter.getItem(viewPager.getCurrentItem())
                .getFragmentManager())
                .getBackStackEntryCount();

        if (d > 0) super.onBackPressed();
        else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, getResources().getString(R.string.back_to_exit), Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
        }
    }

    /**
     * Init view of navigation bar
     */
    private void createBottomNavigationBar() {
        bottomNavigationBar = findViewById(R.id.bottom_navigation_bar);

        homeBadgeItem = new ShapeBadgeItem().setShape(ShapeBadgeItem.SHAPE_OVAL).setShapeColor(getResources().getColor(R.color.color15)).hide();
        newsBadgeItem = new TextBadgeItem().setText("0");
        notifyBadgeItem = new TextBadgeItem().setText("0").hide();

        BottomNavigationItem itemHome = new BottomNavigationItem(R.drawable.ic_home, R.string.home_bar).setBadgeItem(homeBadgeItem);
        BottomNavigationItem itemNews = new BottomNavigationItem(R.drawable.ic_news, R.string.news_bar).setBadgeItem(newsBadgeItem);
        BottomNavigationItem itemNotify = new BottomNavigationItem(R.drawable.ic_notification, R.string.notification_bar).setBadgeItem(notifyBadgeItem);
        BottomNavigationItem itemInfo = new BottomNavigationItem(R.drawable.ic_information, R.string.information_nar);

        bottomNavigationBar.addItem(itemHome)
                .addItem(itemNews)
                .addItem(itemNotify)
                .addItem(itemInfo)
                .setTabSelectedListener(new BottomNavigationBar.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(int position) {
                        viewPager.setCurrentItem(position);
                    }

                    @Override
                    public void onTabUnselected(int position) {

                    }

                    @Override
                    public void onTabReselected(int position) {

                    }
                })
                .initialise();
        bottomNavigationBar.setVisibility(View.INVISIBLE);
        Log.d(TAG, "Init bottom navigation bar success!");
    }

    /**
     * Logout google account
     */
    private void logout() {
        //Logout google account
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(status -> Log.d(TAG, "SIGN_OUT_STATUS: " + status.getStatusCode()));

        //Remove all database
        App.sqlite(this).getAllDaos().forEach(dao -> {
            if (!(dao instanceof StudentDao)) {
                dao.deleteAll();
                Log.d(TAG, dao.getClass().getSimpleName() + " deleted!");
            }
        });

        //Remove all Fragment
        viewPager.setOffscreenPageLimit(0);

        //Back to login page
        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
        finish();
    }

    /**
     * Init Google API Client
     */
    private void googleInit() {
        //Set up login with Google
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        if (googleApiClient == null){
            googleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this, this)
                    .addApi(Auth.GOOGLE_SIGN_IN_API , googleSignInOptions).build();
        }

        Log.d(TAG, "Google Api Client is created!");
    }

    @Override
    public void subjectDownload(int process, int size) {
        if (!isSubjectDownloaded) setProgress(process, size);
        if (process == size) {
            isSubjectDownloaded = true;
            Log.d(TAG + "Download", "Subject");
        }
    }

    @Override
    public void statusDownload(int process, int size) {
        if (!isStatusDownloaded && isClassDownloaded) setProgress(process, size);
        if (process == size) {
            isStatusDownloaded = true;
            Log.d(TAG + "Download", "Status");
        }
    }



    @Override
    public void classDownload(int process, int size) {
        if (!isClassDownloaded && isSubjectDownloaded) setProgress(process, size);
        if (process == size) {
            isClassDownloaded = true;
            Log.d(TAG + "Download", "Class");
        }
    }

    @Override
    public void majorDownload(int process, int size) {
        if (!isMajorDownloaded && isNewsDownloaded) setProgress(process, size);
        if (process == size) {
            isMajorDownloaded = true;
            Log.d(TAG + "Download", "Major");
        }
    }

    @Override
    public void roomDownload(int process, int size) {
        if (!isRoomDownloaded && isMajorDownloaded) setProgress(process, size);
        if (process == size) {
            isRoomDownloaded = true;
            Log.d(TAG + "Download", "Room");
        }
    }

    @Override
    public void slotDownload(int process, int size) {
        if (!isSlotDownloaded && isSessionDownloaded) setProgress(process, size);
        if (process == size) {
            isSlotDownloaded = true;
            Log.d(TAG + "Download", "Slot");
        }
    }

    @Override
    public void sessionDownload(int process, int size) {
        if (!isSessionDownloaded && isSubjectSemesterDownloaded) setProgress(process, size);
        if (process == size) {
            isSessionDownloaded = true;
            Log.d(TAG + "Download", "Session");
        }
    }

    @Override
    public void subjectSemesterDownload(int process, int size) {
        if (!isSubjectSemesterDownloaded && isRoomDownloaded) setProgress(process, size);
        if (process == size) {
            isSubjectSemesterDownloaded = true;
            Log.d(TAG + "Download", "SubjectSemester");
        }
    }

    @Override
    public void studentDownload(int process, int size) {
        if (!isStudentDownloaded && isSlotDownloaded) setProgress(process, size);
        if (process == size) {
            isStudentDownloaded = true;
            Log.d(TAG + "Download", "Student");
        }
    }

    @Override
    public void newsDownload(int process, int size) {
        if (!isNewsDownloaded && isStatusDownloaded) setProgress(process, size);
        if (process == size) {
            isNewsDownloaded = true;
            Log.d(TAG + "Download", "News");
        }
    }

    private long n = 0;

    private void setLoading() {
        int i = 0;
        while (i != WAIT) {
            i += WAIT/100;

            int d = i;
            progressBar.post(() -> {
                progressBar.setMax(WAIT);
                progressBar.setProgress(d);
                DecimalFormat df = new DecimalFormat("0.0");
                String info = "Loading... (" + df.format(d*100.0/WAIT) + "%)";
                txtDownloadInfo.post(() -> txtDownloadInfo.setText(info));
            });

            try {
                Thread.sleep(WAIT/100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        layoutLoading.post(() -> layoutLoading.setVisibility(View.GONE));
        bottomNavigationBar.post(() -> {
            bottomNavigationBar.setVisibility(View.VISIBLE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        });
    }


    private boolean isLoading = true;
    @SuppressLint("StaticFieldLeak")
    private void setProgress(int progress, int size) {
        Boolean[] isSync = {isSubjectDownloaded, isStatusDownloaded, isClassDownloaded, isMajorDownloaded, isNewsDownloaded, isRoomDownloaded, isSubjectSemesterDownloaded, isSlotDownloaded, isStudentDownloaded, isSessionDownloaded};
        if (progress == size) n = Arrays.stream(isSync).filter(aBoolean -> aBoolean).count();
        if (isFirstLogin) {
            DecimalFormat df = new DecimalFormat("0.0");
            String info = "Syncing " + (n+1) + " of " + isSync.length + "... (" + df.format(progress*100.0/size) + "%)";

            progressBar.post(() -> {
                progressBar.setMax(size);
                progressBar.setProgress(progress);
            });
            txtDownloadInfo.post(() -> txtDownloadInfo.setText(info));

            if (n==isSync.length-1 && progress == size) {
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... voids) {
                        setLoading();
                        return null;
                    }
                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                MapEntity mapEntity = new MapEntity("isFirst", "false");
                App.sqlite(HomeActivity.this).insertOrReplace(mapEntity);
            }
        } else if (isLoading) {
            isLoading = false;
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    setLoading();
                    return null;
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    @Override
    public void newsDetailsFrag(boolean isBackPress) {
        if (isBackPress) onBackPressed();
    }

    @Override
    public void subjectFrag(boolean isBackPress) {
        if (isBackPress) onBackPressed();
    }

    @Override
    public void classFrag(boolean isBackPress) {
        if (isBackPress) onBackPressed();
    }

    @Override
    public void attendanceFrag(boolean isBackPress) {
        if (isBackPress) onBackPressed();
    }

    @Override
    public void homeFrag(boolean isOpenSlot) {
        bottomNavigationBar.post(() -> {
            if (!isOpenSlot) homeBadgeItem.hide(true);
            else homeBadgeItem.show(true);
        });
    }

    @Override
    public void studentFrag(boolean isBack) {
        if (isBack) onBackPressed();
    }

    @Override
    public void timetableFrag(boolean isBackPress) {
        if (isBackPress) onBackPressed();
    }
}
