package tech.tplus.akkoattendance;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import tech.tplus.akkoattendance.adapter.EventAdapter;
import tech.tplus.akkoattendance.entity.Clazz;
import tech.tplus.akkoattendance.entity.Room;
import tech.tplus.akkoattendance.entity.Subject;
import tech.tplus.akkoattendance.entity.SubjectSlot;
import tech.tplus.akkoattendance.helper.App;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class TimetableFragment extends Fragment {
    private static final String TAG = TimetableFragment.class.getSimpleName();

    private TimetableFragListener listener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_timetable, container, false);

        TextView txtEvent = v.findViewById(R.id.txt_event);
        CalendarView calendarView = v.findViewById(R.id.calendar_action);
        ListView lvSlot = v.findViewById(R.id.lv_slot);
        ImageView imgBack = v.findViewById(R.id.img_back);

        //Get all slot
        Date d = new Date();

        List<Room> roomList = App.sqlite(Objects.requireNonNull(getActivity())).getRoomDao().loadAll();
        List<Subject> subjectList = App.sqlite(getActivity()).getSubjectDao().loadAll();
        List<Clazz> clazzList = App.sqlite(getActivity()).getClazzDao().loadAll();
        List<SubjectSlot> slots = App.sqlite(getActivity()).getSubjectSlotDao().loadAll();
        List<SubjectSlot> slotToday = slots.stream().filter(slot -> slot.getDate().getDate() == d.getDate() &&
                slot.getDate().getMonth() == d.getMonth() &&
                slot.getDate().getYear() == d.getYear()).collect(Collectors.toList());

        AtomicReference<EventAdapter> adapter = new AtomicReference<>();
        if (slotToday.isEmpty()) txtEvent.setText(getResources().getString(R.string.no_event));
        else {
            adapter.set(new EventAdapter(slotToday, roomList, subjectList, clazzList));
            lvSlot.setAdapter(adapter.get());
            App.setListViewHeightBasedOnChildren(lvSlot);
            lvSlot.setOnItemClickListener((parent, view, position, id) -> {
                assert getFragmentManager() != null;
                getFragmentManager().beginTransaction()
                        .add(R.id.home_container, new StudentFragment((SubjectSlot)parent.getItemAtPosition(position)))
                        .addToBackStack(TAG)
                        .commit();
            });
        }

        //Set view
        imgBack.setOnClickListener(e -> listener.timetableFrag(true));
        calendarView.setOnDateChangeListener((view, year, month, dayOfMonth) -> {
            List<SubjectSlot> slotList = slots.stream().filter(slot -> {
                Calendar c = Calendar.getInstance();
                c.setTime(slot.getDate());
                return c.get(Calendar.DAY_OF_MONTH) == dayOfMonth &&
                        c.get(Calendar.MONTH) == month &&
                        c.get(Calendar.YEAR) == year;
            }).collect(Collectors.toList());
            if (slotList.isEmpty()) txtEvent.setText(getResources().getString(R.string.no_event));
            else txtEvent.setText(getResources().getString(R.string.upcoming_event));

            adapter.set(new EventAdapter(slotList, roomList, subjectList, clazzList));
            lvSlot.setAdapter(adapter.get());
            App.setListViewHeightBasedOnChildren(lvSlot);
            adapter.get().notifyDataSetChanged();
        });

        return v;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof TimetableFragment.TimetableFragListener) {
            listener = (TimetableFragment.TimetableFragListener) context;
        } else {
            Log.d(TAG, "Cannot attach data to HomeActivity");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public interface TimetableFragListener {
        void timetableFrag(boolean isBackPress);
    }
}
