package tech.tplus.akkoattendance.other;

public enum PopupStatus {
    SUCCESS, WARNING, DANGER
}