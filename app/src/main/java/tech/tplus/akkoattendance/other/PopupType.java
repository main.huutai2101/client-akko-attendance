package tech.tplus.akkoattendance.other;

public enum PopupType {
    OK, OK_CANCEL, YES_NO
}