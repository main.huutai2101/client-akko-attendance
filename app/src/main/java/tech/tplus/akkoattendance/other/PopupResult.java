package tech.tplus.akkoattendance.other;

public enum PopupResult {
    OK, YES_OK, NO_CANCEL
}