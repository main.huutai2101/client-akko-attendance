package tech.tplus.akkoattendance;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import tech.tplus.akkoattendance.other.PopupResult;
import tech.tplus.akkoattendance.other.PopupStatus;
import tech.tplus.akkoattendance.other.PopupType;

import java.util.Objects;

public class Popup extends Dialog implements View.OnClickListener{
    private static final String TAG = Popup.class.getSimpleName();

    private final String title;
    private final String message;
    private final PopupStatus popupStatus;
    private final PopupType popupType;

    private PopupInterface popupInterface;

    public Popup(@NonNull Context context, String title, String message, PopupStatus popupStatus, PopupType popupType) {
        super(context);
        this.title = title;
        this.message = message;
        this.popupStatus = popupStatus;
        this.popupType = popupType;
    }

    public Popup setPopupListener(PopupInterface popupInterface) {
        this.popupInterface = popupInterface;
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popup);

        Objects.requireNonNull(getWindow()).setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        setCanceledOnTouchOutside(false);


        ImageView ivIcon = findViewById(R.id.ic_popup);
        TextView tvTitle = findViewById(R.id.txtTitle);
        TextView tvMessage = findViewById(R.id.txtMessage);
        ConstraintLayout layoutOk = findViewById(R.id.layout_ok);
        ConstraintLayout layoutYesNo = findViewById(R.id.layout_yes_no);
        Button btnOk = findViewById(R.id.btn_ok);
        Button btnYes = findViewById(R.id.btn_yes);
        Button btnNo = findViewById(R.id.btn_no);

        //Set data
        tvTitle.setText(title);
        tvMessage.setText(message);

        //Init icon and color title
        switch (popupStatus) {
            case SUCCESS:
                ivIcon.setImageResource(R.drawable.ic_success);
                tvTitle.setTextColor(getContext().getResources().getColor(R.color.colorSuccess));
                break;
            case WARNING:
                ivIcon.setImageResource(R.drawable.ic_warning);
                tvTitle.setTextColor(getContext().getResources().getColor(R.color.colorWarning));
                break;
            case DANGER:
                ivIcon.setImageResource(R.drawable.ic_danger);
                tvTitle.setTextColor(getContext().getResources().getColor(R.color.colorDanger));
                break;
        }

        //Init button
        switch (popupType) {
            case OK:
                layoutYesNo.setVisibility(View.GONE);
                btnOk.setText(R.string.popup_ok);
                break;
            case YES_NO:
                layoutOk.setVisibility(View.GONE);
                btnYes.setText(R.string.popup_yes);
                btnNo.setText(R.string.popup_no);
                break;
            case OK_CANCEL:
                layoutOk.setVisibility(View.GONE);
                btnYes.setText(R.string.popup_ok);
                btnNo.setText(R.string.popup_cancel);
                break;
        }

        //Init event
        btnNo.setOnClickListener(this);
        btnOk.setOnClickListener(this);
        btnYes.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_ok:
                if (popupInterface != null) popupInterface.onResult(PopupResult.OK);
                break;
            case R.id.btn_yes:
                if (popupInterface != null) popupInterface.onResult(PopupResult.YES_OK);
                break;
            case R.id.btn_no:
                if (popupInterface != null) popupInterface.onResult(PopupResult.NO_CANCEL);
                break;
        }
        dismiss();
    }

    @Override
    public void dismiss() {
        Log.d(TAG, "Popup is closed!");
        super.dismiss();
    }

    @Override
    public void onBackPressed() { }

    public interface PopupInterface {
        void onResult(PopupResult result);
    }
}