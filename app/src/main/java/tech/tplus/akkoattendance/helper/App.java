package tech.tplus.akkoattendance.helper;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import androidx.core.app.ActivityCompat;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tech.tplus.akkoattendance.controller.DownloadController;
import tech.tplus.akkoattendance.entity.DaoSession;

import java.io.*;
import java.util.Arrays;

import static android.content.Context.CONNECTIVITY_SERVICE;

public class App {
    private static final String TAG = App.class.getSimpleName();
    private static final int PERMISSIONS_REQUEST = 1;
    private static final String[] PERMISSIONS = {
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.INTERNET,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
    };


    /**
     * Check Permission of Application
     * @param activity Activity is running
     */
    public static void checkPermission(Activity activity) {
        Arrays.stream(PERMISSIONS).forEach(s -> {
            if (activity.getApplicationContext().checkSelfPermission(s) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions (activity, PERMISSIONS, PERMISSIONS_REQUEST);
            }
        });
        Log.d(TAG, "Check Permissions for Application!");
    }

    /**
     * Get data from SQLite
     * @param activity Activity is running
     */
    public static DaoSession sqlite(Activity activity) {
        return ((DB) activity.getApplication()).session();
    }

    /**
     * Check Network is connected
     * @param activity Activity is running
     */
    public static boolean isNetworkConnected(Activity activity) {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(CONNECTIVITY_SERVICE);
        assert cm != null;
        boolean isConnect = cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
        Log.d(TAG, "Is Network Connected: " + isConnect);
        return isConnect;
    }

    /**
     * Save file
     * @param data data to save
     * @param file file uri
     * @return uri
     */
    private static Uri saveFile(ResponseBody data, File file) {
        try {
            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];
                inputStream = data.byteStream();
                outputStream = new FileOutputStream(file);
                while (true) {
                    int read = inputStream.read(fileReader);
                    if (read == -1) break;
                    outputStream.write(fileReader, 0, read);
                }
                outputStream.flush();
                return Uri.fromFile(file);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            } finally {
                if (inputStream != null) inputStream.close();
                if (outputStream != null) outputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Download and save data. Must be using with AsyncTask when call this function
     * @param activity activity is running
     * @param url url of data
     * @param folder folder to storage
     * @return url
     */
    public static Uri downloadData(Activity activity, String url, String folder) {
        //Get URI download and file name
        String uri = url.substring(url.lastIndexOf("download")+9).replace("/","-");
        String fileName = uri.substring(uri.indexOf('-')+1);

        //Create temp file to return Uri
        File directory = new File(activity.getFilesDir() + File.separator + folder);
        if (!directory.exists()) directory.mkdir();
        File file = new File(directory, fileName);

        final boolean[] isResponse = {false};
        DownloadController downloadController = API.build(activity).create(DownloadController.class);
        downloadController.get(uri).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    Uri dataUri = saveFile(response.body(), file);
                    assert dataUri != null;
                    Log.d(TAG, "Data save to Uri: " + dataUri.toString());
                } else {
                    Log.d(TAG, "Unable to connect to download data in server!");
                }
                isResponse[0] = true;
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                isResponse[0] = true;
                Log.d(TAG, "Unable to connect to the Server!");
            }
        });

        //Waiting for downloading and saving file
        while (!isResponse[0]) {
            try {
                Log.d(TAG, "Downloading file: " + file.getPath() + "...");
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return Uri.fromFile(file);
    }


    /**
     * Disable scroll in list view and set height with NestedSrollView
     * @param listView a list data
     */
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight=0;
        View view = null;

        for (int i = 0; i < listAdapter.getCount(); i++)
        {
            view = listAdapter.getView(i, view, listView);

            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth,
                        LinearLayout.LayoutParams.MATCH_PARENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();

        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + ((listView.getDividerHeight()) * (listAdapter.getCount()));

        listView.setLayoutParams(params);
        listView.requestLayout();
    }
}
