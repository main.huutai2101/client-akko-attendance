package tech.tplus.akkoattendance.helper;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import tech.tplus.akkoattendance.R;


import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


final class Config {
    private static final String TAG = Config.class.getSimpleName();

    static String getConfigValue(Context context, String name) {
        name = name.toUpperCase();
        Log.d(TAG, "Get config: " + name);
        Resources resources = context.getResources();
        try {
            InputStream rawResource = resources.openRawResource(R.raw.config);
            Properties properties = new Properties();
            properties.load(rawResource);
            return properties.getProperty(name);
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Unable to find the config file: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "Failed to open config file.");
        }

        return null;
    }
}
