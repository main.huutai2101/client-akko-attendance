package tech.tplus.akkoattendance.helper;

import android.app.Application;
import android.util.Log;

import org.greenrobot.greendao.database.Database;
import tech.tplus.akkoattendance.entity.DaoMaster;
import tech.tplus.akkoattendance.entity.DaoSession;

import java.util.Objects;


public class DB extends Application {
    private static  final String TAG = DB.class.getSimpleName();
    private DaoSession mDaoSession;

    @Override
    public void onCreate() {
        super.onCreate();

        //Get Config
        String dbName = Config.getConfigValue(this,"DB_NAME");
        boolean createDrop = Boolean.parseBoolean(Objects.requireNonNull(Config.getConfigValue(this, "CREATE_DROP")));
        Log.d(TAG, "DATABASE_NAME: " + dbName);

        //Init database
        Database mDatabase = new DBHelper(this, dbName).getWritableDb();


        //Drop old database and create new
        if (createDrop) {
            DaoMaster.dropAllTables(mDatabase, true);
            DaoMaster.createAllTables(mDatabase, true);
        }

        //Get new Session
        DaoMaster mDaoMaster = new DaoMaster(mDatabase);
        mDaoSession = mDaoMaster.newSession();
    }

    public DaoSession session() {
        return mDaoSession;
    }
}
