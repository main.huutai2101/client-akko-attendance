package tech.tplus.akkoattendance.helper;

import android.content.Context;
import android.util.Log;

import java.security.Security;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import org.conscrypt.Conscrypt;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import tech.tplus.akkoattendance.BuildConfig;

public class API {
    private static final String TAG = API.class.getSimpleName();
    private static  Retrofit retrofit = null;

    public static Retrofit build(Context context) {
        if (retrofit == null) config(context);
        return retrofit;
    }

    private static void config(Context context) {
        //Get config in config.properties
        String host = Config.getConfigValue(context,"HOST");
        String port = Config.getConfigValue(context,"PORT");
        String urlBase = "http://" + host + ':' + port;
        int readTimeout = Integer.parseInt(Objects.requireNonNull(Config.getConfigValue(context, "READ_TIMEOUT")));
        int connectTimeout =Integer.parseInt(Objects.requireNonNull(Config.getConfigValue(context, "CONNECT_TIMEOUT")));

        //Debug mode
        Log.d(TAG, "URL_BASE: " + urlBase);
        Log.d(TAG, "READ_TIMEOUT: " + readTimeout);
        Log.d(TAG, "CONNECT_TIMEOUT: " + connectTimeout);

        //Init read timeout and connect timeout
        Security.insertProviderAt(Conscrypt.newProvider(), 1);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(readTimeout, TimeUnit.MILLISECONDS)
                .connectTimeout(connectTimeout, TimeUnit.MILLISECONDS)
                .build();

        //Build Retrofit
        retrofit = new Retrofit.Builder()
                .baseUrl(urlBase)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }
}
