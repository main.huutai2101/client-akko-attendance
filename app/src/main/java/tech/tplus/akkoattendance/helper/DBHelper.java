package tech.tplus.akkoattendance.helper;

import android.content.Context;
import android.util.Log;

import org.greenrobot.greendao.database.Database;
import tech.tplus.akkoattendance.entity.DaoMaster;


public class DBHelper extends DaoMaster.DevOpenHelper {
    private static final String TAG = DBHelper.class.getSimpleName();

    public DBHelper(Context context, String name) {
        super(context, name);
    }

    @Override
    public void onUpgrade(Database db, int oldVersion, int newVersion) {
        super.onUpgrade(db, oldVersion, newVersion);
        Log.d(TAG, "DB_OLD_VERSION : " + oldVersion + ", DB_NEW_VERSION : " + newVersion);
        switch (oldVersion) {
            case 1:
                break;
            case 2:
                break;
            default:
        }
    }
}
