package tech.tplus.akkoattendance;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import androidx.annotation.NonNull;
import tech.tplus.akkoattendance.other.PopupResult;

import java.util.Objects;

public class DatePickerPopup extends Dialog implements View.OnClickListener{

    private final DataPickerInterface dataPickerInterface;
    private int year;
    private int monthOfYear;
    private int dayOfMonth;


    public DatePickerPopup(@NonNull Context context, String currentSelected, DataPickerInterface dataPickerInterface) {
        super(context);
        this.dataPickerInterface = dataPickerInterface;
        String[] ls = currentSelected.split("/");
        dayOfMonth = Integer.parseInt(ls[0]);
        monthOfYear = Integer.parseInt(ls[1]);
        year = Integer.parseInt(ls[2]);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.date_picker);

        Objects.requireNonNull(getWindow()).setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        DatePicker datePicker = findViewById(R.id.date_picker);
        datePicker.updateDate(year, monthOfYear-1, dayOfMonth);
        datePicker.setOnDateChangedListener((view, year, monthOfYear, dayOfMonth) -> {
            this.year = year;
            this.dayOfMonth = dayOfMonth;
            this.monthOfYear = monthOfYear;
        });

        Button btnOk = findViewById(R.id.btn_yes);
        Button btnCancel = findViewById(R.id.btn_no);

        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_yes:
                dataPickerInterface.onResult(PopupResult.YES_OK, year, monthOfYear, dayOfMonth);
                break;
            case R.id.btn_no:
                dataPickerInterface.onResult(PopupResult.NO_CANCEL, 0, 0, 0);
                break;
        }
        dismiss();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        dataPickerInterface.onResult(PopupResult.NO_CANCEL, 0, 0, 0);
    }

    @Override
    public void setCanceledOnTouchOutside(boolean cancel) {
        super.setCanceledOnTouchOutside(cancel);
        dataPickerInterface.onResult(PopupResult.NO_CANCEL, 0, 0, 0);
    }

    public interface DataPickerInterface {
        void onResult(PopupResult result, int year, int monthOfYear, int dayOfMonth);
    }
}